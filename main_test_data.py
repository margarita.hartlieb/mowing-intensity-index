"""
Program to calculate the impact of Mowing depending on various variables for Farm Sites.

This script processes mowing data to evaluate the impact of various factors on mowing performance.
It involves cleaning the data, handling missing values, and computing a measure of impact (M(i))
based on different mowing parameters such as Mowing Machine, Cut Width, and Cut Height.
It also generates various statistical analyses and visualizations.

The main steps are:
1. Load required packages.
2. Set parameters.
3. Load and clean the data.
4. Calculate M(i) values.
5. Plot histograms of M(i).
6. Plot single variable trends.
7. Plot annual trends.
8. Output statistical summaries.

Functions:
- clean_data_set: Cleans the data, handling missing values and filtering by specified years.
- set_mowing_machine_: Converts mowing machine values.
- set_conditioner_: Converts conditioner values.
- set_cut_width_: Converts cut width values.
- set_cut_heigth_: Converts cut height values.

Parameters:
- data_path: Path to the data file.
- workingtitle: Title for the working set of results.
- excel_sheet: Sheet number in the Excel file (if applicable).
- take_average_for_comparison: Whether to use the average for comparison.
- use_std_dev_for_normalizing: Whether to use standard deviation for normalizing.
- include_cutwdith: Whether to include cut width in calculations.
- set_weighting: Whether to use custom weighting parameters.
- years: List of years to filter the data by.
- keep_everything: Whether to keep all data regardless of missing values.
- alpha_*: Weighting parameters for different mowing attributes.
"""
########################################################################################################################
# START # LOAD PACKAGES ################################################################################################
########################################################################################################################
import numpy as np
import pandas as pd
import scipy
import sys
from sklearn import metrics
from copy import deepcopy as dc

import func_Mi
from func_data_prep import clean_data_set, augment_consecutive_data_
from func_stats import *
from matplotlib import pyplot as plt
from func_Mi import new_Mi_range, new_Mi_range_no_width, old_Mi, new_Mi_range_no_width_no_conditioner
from func_trends import annual_trend, annual_trend_regional, annual_trend_single, annual_trend_single_variable
from func_orga import create_folder_structure
from matplotlib.pyplot import cm
########################################################################################################################
## END ## LOAD PACKAGES ################################################################################################
########################################################################################################################
#
#
#
#
#
#
#
#
#
#
########################################################################################################################
# START # SET PARAMETERS ###############################################################################################
########################################################################################################################
data_path = "./translate_data/data_cleaned_SR_csv.csv" #Needs to be set

#workingtitle = "run_2008_2018_nb2"
#workingtitle = "run_2006_2021_nb2"
workingtitle = "test_data2022_analysis"
#workingtitle = "alt_weighting_1_no_cutwidth"

take_average_for_comparison = False #Needs to be set
use_std_dev_for_normalizing = False #Needs to be set
include_cutwdith = False
include_conditioner = False
set_weighting = True
years = [2008, 2018] #if no years are specified set years = None
years = None #if no years are specified set years = None
keep_everything = True #this parameter keeps all data even if not enough details available and sets everything to the neutral benchmark. Only cuts needs to be available.
alpha_MowingMachine = 1.0 #default 1
alpha_CutWidth = -1.0 #default -1
alpha_CutHeight = -1.0 #default -1
alpha_MowingConditioner = 2 #default 1
params = [alpha_MowingMachine, alpha_CutWidth, alpha_CutHeight, alpha_MowingMachine]

# change output according to parametrization
# Change output according to parametrization
add_out = ""
if take_average_for_comparison:
    add_out += "_average"
if use_std_dev_for_normalizing:
    add_out += "_stddev"
if include_cutwdith:
    add_out += "_includecutwidth"
if years is not None:
    add_out += f"_years{years[0]}-{years[1]}"
if keep_everything:
    add_out += "_keepeverything"
print('Specs:')
print(workingtitle)
print(data_path)
print('')
create_folder_structure(workingtitle)
########################################################################################################################
## END ## SET PARAMETERS ###############################################################################################
########################################################################################################################
#
#
#
#
#
#
#
#
#
#
########################################################################################################################
# START # LOAD AND CLEAN DATA ##########################################################################################
########################################################################################################################
print('Load data')
#load data
# Load data correctly
data = pd.read_csv(data_path)

# Do whatever you need with your data
print(data.head())
#get needed columns
data = dc(data[["EP_PlotID", "Year", "MowingMachine1", "CutWidth_m1", "CutHeight_cm1", "MowingConditioner1", "Cuts"]])
#get rid off bad data points and convert to ordinal scaled values
data = dc(clean_data_set(data, years=years, keep_everything=keep_everything))

# Debug: Check final cleaned data
print("Final cleaned data:")
print(data.head())
print('jsadhjsdfhsdj')
##compensate for missing values
data_clean = augment_consecutive_data_(data)
#save for comparison
data_clean.to_csv('./DATA/' + workingtitle + '/ordinal_dataset.csv', index=False)
########################################################################################################################
## END ## LOAD AND CLEAN DATA ##########################################################################################
########################################################################################################################
#
#
#
#
#
#
#
#
#
#
########################################################################################################################
# START # CALCULATE M(i) ###############################################################################################
########################################################################################################################
print('Calculate M(i)')
#calculate M(i)s, old and new
Mi_old = old_Mi(data_clean)
if include_cutwdith: #include cutwidth for calculating the M(i), yes/no
    if set_weighting: #Set custom parameters yes/no
        Mi_new = new_Mi_range(data_clean, params=params, keep_everything=keep_everything)
    else:
        Mi_new = new_Mi_range(data_clean, keep_everything=keep_everything)
else:
    if include_conditioner:
        if set_weighting:
            Mi_new = new_Mi_range_no_width(data_clean, params=params, keep_everything=keep_everything)
        else:
            Mi_new = new_Mi_range_no_width(data_clean, keep_everything=keep_everything)
    else:
        if set_weighting:
            Mi_new = new_Mi_range_no_width_no_conditioner(data_clean, params=params, keep_everything=keep_everything)
        else:
            Mi_new = new_Mi_range_no_width_no_conditioner(data_clean, keep_everything=keep_everything)

Years_Mi = np.array(data_clean["Year"].to_numpy(), dtype=object)
EP_PlotID_Mi = np.array(data_clean["EP_PlotID"].to_numpy(), dtype=object)
print(len(Mi_old))
print(len(Mi_new))
print(len(Years_Mi))
print(len(EP_PlotID_Mi))
#sys.exit()
#save for comparison
header_out = list()
header_out.append('Year')
header_out.append('EP_PlotID')
header_out.append('Mi_old')
header_out.append('Mi_new')
out_arr_Mi = np.empty((len(Mi_old)+1, 4), dtype=object)
out_arr_Mi[0,:] = dc(np.array(header_out, dtype=object))
out_arr_Mi[1:,0] = dc(Years_Mi)
out_arr_Mi[1:,1] = dc(EP_PlotID_Mi)
out_arr_Mi[1:,2] = dc(Mi_old)
out_arr_Mi[1:,3] = dc(Mi_new)


np.savetxt('./DATA/' + workingtitle + '/Mi_all_data.csv', out_arr_Mi, fmt='%s', delimiter=",")
np.savetxt('./DATA/' + workingtitle + '/Mi_old' + add_out + '.csv', Mi_old, fmt='%s', delimiter=",")
np.savetxt('./DATA/' + workingtitle + '/Mi_new' + add_out + '.csv', Mi_new, fmt='%s', delimiter=",")


#sys.exit()
########################################################################################################################
## END ## CALCULATE M(i) ###############################################################################################
########################################################################################################################
#
#
#
#
#
#
#
#
#
#
########################################################################################################################
# START # PLOT HISTOGRAMS ##############################################################################################
########################################################################################################################
print('Plot histograms')
#define bins for histograms
bins = [-1,0,1,2,3,4,5,6]
bins_fg = [-0.25,0,0.25,0.5,0.75,1.0,1.25,1.5,1.75,2.0,2.25,2.5,2.75,3.0,3.25,3.5,3.75,4.0,4.25,4.5,4.75,5.0,5.25,5.5,5.75,6.0]
#plot regular histogram M(i) old
plt.hist(Mi_old, bins, color="orange")
plt.xlabel('M(i) old')
plt.savefig('./PLOTS/' + workingtitle + '/histo_mi_old_' + workingtitle +  add_out + '.png')  # , dpi=150)
plt.savefig('./PLOTS/' + workingtitle + '/histo_mi_old_' + workingtitle +  add_out + '.eps')  # , dpi=150)
plt.ion()
plt.show(block=False)
plt.pause(0.01)
plt.close()
#plot regular histogram M(i) new
plt.hist(Mi_new, bins, color="orange")
plt.xlabel('M(i) new')
plt.savefig('./PLOTS/' + workingtitle + '/histo_mi_new_' + workingtitle +  add_out + '.png')
plt.savefig('./PLOTS/' + workingtitle + '/histo_mi_new_' + workingtitle +  add_out + '.eps')  # , dpi=150)
plt.ion()
plt.show(block=False)
plt.pause(0.01)
plt.close()
#plot fine-grained histogram M(i) old
plt.hist(Mi_old, bins_fg, color="orange")
plt.xlabel('M(i) old')
plt.savefig('./PLOTS/' + workingtitle + '/histo_fine_mi_old_' + workingtitle +  add_out + '.png')  # , dpi=150)
plt.savefig('./PLOTS/' + workingtitle + '/histo_fine_mi_old_' + workingtitle +  add_out + '.eps')  # , dpi=150)
plt.ion()
plt.show(block=False)
plt.pause(0.01)
plt.close()
#plot fine-grained histogram M(i) new
plt.hist(Mi_new, bins_fg, color="orange")
plt.xlabel('M(i) new')
plt.savefig('./PLOTS/' + workingtitle + '/histo_fine_mi_new_' + workingtitle +  add_out + '.png')
plt.savefig('./PLOTS/' + workingtitle + '/histo_fine_mi_new_' + workingtitle +  add_out + '.eps')
plt.ion()
plt.show(block=False)
plt.pause(0.01)
plt.close()
#plot regular histogram and adding gaussian fit M(i) old
best_fit_line_old = scipy.stats.norm.pdf(bins, np.mean(Mi_old), np.std(Mi_old))
plt.hist(Mi_old, bins, density=1, alpha=0.5)
plt.plot(bins, best_fit_line_old)
plt.xlabel('M(i) old')
plt.savefig('./PLOTS/' + workingtitle + '/histo_gauss_mi_old_' + workingtitle +  add_out + '.png')
plt.savefig('./PLOTS/' + workingtitle + '/histo_gauss_mi_old_' + workingtitle +  add_out + '.eps')
plt.ion()
plt.show(block=False)
plt.pause(0.01)
plt.close()
#plot regular histogram and adding gaussian fit M(i) new
best_fit_line_new = scipy.stats.norm.pdf(bins, np.mean(Mi_new), np.std(Mi_new))
plt.hist(Mi_new, bins, density=1, alpha=0.5)
plt.plot(bins, best_fit_line_new)
plt.xlabel('M(i) new')
plt.savefig('./PLOTS/' + workingtitle + '/histo_gauss_mi_new_' + workingtitle +  add_out + '.png')
plt.savefig('./PLOTS/' + workingtitle + '/histo_gauss_mi_new_' + workingtitle +  add_out + '.eps')
plt.ion()
plt.show(block=False)
plt.pause(0.01)
plt.close()
#plot fine-grained histogram and adding gaussian fit M(i) old
best_fit_line_old_fg = scipy.stats.norm.pdf(bins_fg, np.mean(Mi_old), np.std(Mi_old))
plt.hist(Mi_old, bins_fg, density=1, alpha=0.5)
plt.plot(bins_fg, best_fit_line_old_fg)
plt.xlabel('M(i) old')
plt.savefig('./PLOTS/' + workingtitle + '/histo_gauss_fine_mi_old_' + workingtitle +  add_out + '.png')
plt.savefig('./PLOTS/' + workingtitle + '/histo_gauss_fine_mi_old_' + workingtitle +  add_out + '.eps')
plt.ion()
plt.show(block=False)
plt.pause(0.01)
plt.close()
#plot fine-grained histogram and adding gaussian fit M(i) new
best_fit_line_new_fg = scipy.stats.norm.pdf(bins_fg, np.mean(Mi_new), np.std(Mi_new))
plt.hist(Mi_new, bins_fg, density=1, alpha=0.5)
plt.plot(bins_fg, best_fit_line_new_fg)
plt.xlabel('M(i) new')
plt.savefig('./PLOTS/' + workingtitle + '/histo_gauss_fine_mi_new_' + workingtitle +  add_out + '.png')
plt.savefig('./PLOTS/' + workingtitle + '/histo_gauss_fine_mi_new_' + workingtitle +  add_out + '.eps')
plt.ion()
plt.show(block=False)
plt.pause(0.01)
plt.close()

