"""
Program to calculate the impact of Mowing depending on various variables for Farm Sites.

This script processes mowing data to evaluate the impact of various factors on mowing performance.
It involves cleaning the data, handling missing values, and computing a measure of impact (M(i))
based on different mowing parameters such as Mowing Machine, Cut Width, and Cut Height.
It also generates various statistical analyses and visualizations.

The main steps are:
1. Load required packages.
2. Set parameters.
3. Load and clean the data.
4. Calculate M(i) values.
5. Plot histograms of M(i).
6. Plot single variable trends.
7. Plot annual trends.
8. Output statistical summaries.

Functions:
- clean_data_set: Cleans the data, handling missing values and filtering by specified years.
- set_mowing_machine_: Converts mowing machine values.
- set_conditioner_: Converts conditioner values.
- set_cut_width_: Converts cut width values.
- set_cut_heigth_: Converts cut height values.

Parameters:
- data_path: Path to the data file.
- workingtitle: Title for the working set of results.
- excel_sheet: Sheet number in the Excel file (if applicable).
- take_average_for_comparison: Whether to use the average for comparison.
- use_std_dev_for_normalizing: Whether to use standard deviation for normalizing.
- include_cutwdith: Whether to include cut width in calculations.
- set_weighting: Whether to use custom weighting parameters.
- years: List of years to filter the data by.
- keep_everything: Whether to keep all data regardless of missing values.
- alpha_*: Weighting parameters for different mowing attributes.
"""
########################################################################################################################
# START # LOAD PACKAGES ################################################################################################
########################################################################################################################
import numpy as np
import pandas as pd
import scipy
import sys
from sklearn import metrics
from copy import deepcopy as dc

import func_Mi
from func_data_prep import clean_data_set, augment_consecutive_data_
from func_stats import *
from matplotlib import pyplot as plt
from func_Mi import new_Mi_range, new_Mi_range_no_width, old_Mi, new_Mi_range_no_width_no_conditioner
from func_trends import annual_trend, annual_trend_regional, annual_trend_single, annual_trend_single_variable
from func_orga import create_folder_structure
from matplotlib.pyplot import cm
########################################################################################################################
## END ## LOAD PACKAGES ################################################################################################
########################################################################################################################
#
#
#
#
#
#
#
#
#
#
########################################################################################################################
# START # SET PARAMETERS ###############################################################################################
########################################################################################################################
data_path = "./DATA//ORIGINAL_DATA//data_13_09_2022.csv" #Needs to be set

#workingtitle = "run_2008_2018_nb2"
#workingtitle = "run_2006_2021_nb2"
workingtitle = "all_over_alt_weighting_MM05CH05Cond2"
#workingtitle = "alt_weighting_1_no_cutwidth"

take_average_for_comparison = False #Needs to be set
use_std_dev_for_normalizing = False #Needs to be set
include_cutwdith = False
include_conditioner = True
set_weighting = True
years = [2008, 2018] #if no years are specified set years = None
years = None #if no years are specified set years = None
keep_everything = False #this parameter keeps all data even if not enough details available and sets everything to the neutral benchmark. Only cuts needs to be available.
alpha_MowingMachine = 0.5 #default 1
alpha_CutWidth = -1 #default -1
alpha_CutHeight = -0.5 #default -1
alpha_MowingConditioner = 2 #default 1
params = [alpha_MowingMachine, alpha_CutWidth, alpha_CutHeight, alpha_MowingMachine]

# change output according to parametrization
# Change output according to parametrization
add_out = ""
if take_average_for_comparison:
    add_out += "_average"
if use_std_dev_for_normalizing:
    add_out += "_stddev"
if include_cutwdith:
    add_out += "_includecutwidth"
if years is not None:
    add_out += f"_years{years[0]}-{years[1]}"
if keep_everything:
    add_out += "_keepeverything"
print('Specs:')
print(workingtitle)
print(data_path)
print('')
create_folder_structure(workingtitle)
########################################################################################################################
## END ## SET PARAMETERS ###############################################################################################
########################################################################################################################
#
#
#
#
#
#
#
#
#
#
########################################################################################################################
# START # LOAD AND CLEAN DATA ##########################################################################################
########################################################################################################################
print('Load data')
#load data
# Load data correctly
data = pd.read_csv(data_path)

# Do whatever you need with your data
print(data.head())
#get needed columns
data = dc(data[["EP_PlotID", "Year", "MowingMachine1", "CutWidth_m1", "CutHeight_cm1", "MowingConditioner1",  "MowingMachine2", "CutWidth_m2", "CutHeight_cm2", "MowingConditioner2",  "MowingMachine3", "CutWidth_m3", "CutHeight_cm3", "MowingConditioner3",  "MowingMachine4", "CutWidth_m4", "CutHeight_cm4", "MowingConditioner4",  "MowingMachine5", "CutWidth_m5", "CutHeight_cm5", "MowingConditioner5", "Cuts"]])
#get rid off bad data points and convert to ordinal scaled values
data = dc(clean_data_set(data, years=years, keep_everything=keep_everything))
##compensate for missing values
data_clean = augment_consecutive_data_(data)
#save for comparison
data_clean.to_csv('./DATA/' + workingtitle + '/ordinal_dataset.csv', index=False)
########################################################################################################################
## END ## LOAD AND CLEAN DATA ##########################################################################################
########################################################################################################################
#
#
#
#
#
#
#
#
#
#
########################################################################################################################
# START # CALCULATE M(i) ###############################################################################################
########################################################################################################################
print('Calculate M(i)')
#calculate M(i)s, old and new
Mi_old = old_Mi(data_clean)
if include_cutwdith: #include cutwidth for calculating the M(i), yes/no
    if set_weighting: #Set custom parameters yes/no
        Mi_new = new_Mi_range(data_clean, params=params, keep_everything=keep_everything)
    else:
        Mi_new = new_Mi_range(data_clean, keep_everything=keep_everything)
else:
    if include_conditioner:
        if set_weighting:
            Mi_new = new_Mi_range_no_width(data_clean, params=params, keep_everything=keep_everything)
        else:
            Mi_new = new_Mi_range_no_width(data_clean, keep_everything=keep_everything)
    else:
        if set_weighting:
            Mi_new = new_Mi_range_no_width_no_conditioner(data_clean, params=params, keep_everything=keep_everything)
        else:
            Mi_new = new_Mi_range_no_width_no_conditioner(data_clean, keep_everything=keep_everything)

Years_Mi = np.array(data_clean["Year"].to_numpy(), dtype=object)
EP_PlotID_Mi = np.array(data_clean["EP_PlotID"].to_numpy(), dtype=object)
print(len(Mi_old))
print(len(Mi_new))
print(len(Years_Mi))
print(len(EP_PlotID_Mi))
#sys.exit()
#save for comparison
header_out = list()
header_out.append('Year')
header_out.append('EP_PlotID')
header_out.append('Mi_old')
header_out.append('Mi_new')
out_arr_Mi = np.empty((len(Mi_old)+1, 4), dtype=object)
out_arr_Mi[0,:] = dc(np.array(header_out, dtype=object))
out_arr_Mi[1:,0] = dc(Years_Mi)
out_arr_Mi[1:,1] = dc(EP_PlotID_Mi)
out_arr_Mi[1:,2] = dc(Mi_old)
out_arr_Mi[1:,3] = dc(Mi_new)

out_arr_Mi_A = func_Mi.sort_Mi_regional(out_arr_Mi, "A", header_out)
Mi_old_A = dc(out_arr_Mi_A[1:,2])
Mi_new_A = dc(out_arr_Mi_A[1:,3])
out_arr_Mi_H = func_Mi.sort_Mi_regional(out_arr_Mi, "H", header_out)
Mi_old_H = dc(out_arr_Mi_H[1:,2])
Mi_new_H = dc(out_arr_Mi_H[1:,3])
out_arr_Mi_S = func_Mi.sort_Mi_regional(out_arr_Mi, "S", header_out)
Mi_old_S = dc(out_arr_Mi_S[1:,2])
Mi_new_S = dc(out_arr_Mi_S[1:,3])

np.savetxt('./DATA/' + workingtitle + '/Mi_all_data.csv', out_arr_Mi, fmt='%s', delimiter=",")
np.savetxt('./DATA/' + workingtitle + '/Mi_old' + add_out + '.csv', Mi_old, fmt='%s', delimiter=",")
np.savetxt('./DATA/' + workingtitle + '/Mi_new' + add_out + '.csv', Mi_new, fmt='%s', delimiter=",")

np.savetxt('./DATA/' + workingtitle + '/Mi_all_data_A.csv', out_arr_Mi_A, fmt='%s', delimiter=",")
np.savetxt('./DATA/' + workingtitle + '/Mi_old_A' + add_out + '.csv', Mi_old_A, fmt='%s', delimiter=",")
np.savetxt('./DATA/' + workingtitle + '/Mi_new_A' + add_out + '.csv', Mi_new_A, fmt='%s', delimiter=",")

np.savetxt('./DATA/' + workingtitle + '/Mi_all_data_H.csv', out_arr_Mi_H, fmt='%s', delimiter=",")
np.savetxt('./DATA/' + workingtitle + '/Mi_old_H' + add_out + '.csv', Mi_old_H, fmt='%s', delimiter=",")
np.savetxt('./DATA/' + workingtitle + '/Mi_new_H' + add_out + '.csv', Mi_new_H, fmt='%s', delimiter=",")

np.savetxt('./DATA/' + workingtitle + '/Mi_all_data_S.csv', out_arr_Mi_S, fmt='%s', delimiter=",")
np.savetxt('./DATA/' + workingtitle + '/Mi_old_S' + add_out + '.csv', Mi_old_S, fmt='%s', delimiter=",")
np.savetxt('./DATA/' + workingtitle + '/Mi_new_S' + add_out + '.csv', Mi_new_S, fmt='%s', delimiter=",")


#sys.exit()
########################################################################################################################
## END ## CALCULATE M(i) ###############################################################################################
########################################################################################################################
#
#
#
#
#
#
#
#
#
#
########################################################################################################################
# START # PLOT HISTOGRAMS ##############################################################################################
########################################################################################################################
print('Plot histograms')
#define bins for histograms
bins = [-1,0,1,2,3,4,5,6]
bins_fg = [-0.25,0,0.25,0.5,0.75,1.0,1.25,1.5,1.75,2.0,2.25,2.5,2.75,3.0,3.25,3.5,3.75,4.0,4.25,4.5,4.75,5.0,5.25,5.5,5.75,6.0]
#plot regular histogram M(i) old
plt.hist(Mi_old, bins, color="orange")
plt.xlabel('M(i) old')
plt.savefig('./PLOTS/' + workingtitle + '/histo_mi_old_' + workingtitle +  add_out + '.png')  # , dpi=150)
plt.savefig('./PLOTS/' + workingtitle + '/histo_mi_old_' + workingtitle +  add_out + '.eps')  # , dpi=150)
plt.ion()
plt.show(block=False)
plt.pause(0.01)
plt.close()
#plot regular histogram M(i) new
plt.hist(Mi_new, bins, color="orange")
plt.xlabel('M(i) new')
plt.savefig('./PLOTS/' + workingtitle + '/histo_mi_new_' + workingtitle +  add_out + '.png')
plt.savefig('./PLOTS/' + workingtitle + '/histo_mi_new_' + workingtitle +  add_out + '.eps')  # , dpi=150)
plt.ion()
plt.show(block=False)
plt.pause(0.01)
plt.close()
#plot fine-grained histogram M(i) old
plt.hist(Mi_old, bins_fg, color="orange")
plt.xlabel('M(i) old')
plt.savefig('./PLOTS/' + workingtitle + '/histo_fine_mi_old_' + workingtitle +  add_out + '.png')  # , dpi=150)
plt.savefig('./PLOTS/' + workingtitle + '/histo_fine_mi_old_' + workingtitle +  add_out + '.eps')  # , dpi=150)
plt.ion()
plt.show(block=False)
plt.pause(0.01)
plt.close()
#plot fine-grained histogram M(i) new
plt.hist(Mi_new, bins_fg, color="orange")
plt.xlabel('M(i) new')
plt.savefig('./PLOTS/' + workingtitle + '/histo_fine_mi_new_' + workingtitle +  add_out + '.png')
plt.savefig('./PLOTS/' + workingtitle + '/histo_fine_mi_new_' + workingtitle +  add_out + '.eps')
plt.ion()
plt.show(block=False)
plt.pause(0.01)
plt.close()
#plot regular histogram and adding gaussian fit M(i) old
best_fit_line_old = scipy.stats.norm.pdf(bins, np.mean(Mi_old), np.std(Mi_old))
plt.hist(Mi_old, bins, density=1, alpha=0.5)
plt.plot(bins, best_fit_line_old)
plt.xlabel('M(i) old')
plt.savefig('./PLOTS/' + workingtitle + '/histo_gauss_mi_old_' + workingtitle +  add_out + '.png')
plt.savefig('./PLOTS/' + workingtitle + '/histo_gauss_mi_old_' + workingtitle +  add_out + '.eps')
plt.ion()
plt.show(block=False)
plt.pause(0.01)
plt.close()
#plot regular histogram and adding gaussian fit M(i) new
best_fit_line_new = scipy.stats.norm.pdf(bins, np.mean(Mi_new), np.std(Mi_new))
plt.hist(Mi_new, bins, density=1, alpha=0.5)
plt.plot(bins, best_fit_line_new)
plt.xlabel('M(i) new')
plt.savefig('./PLOTS/' + workingtitle + '/histo_gauss_mi_new_' + workingtitle +  add_out + '.png')
plt.savefig('./PLOTS/' + workingtitle + '/histo_gauss_mi_new_' + workingtitle +  add_out + '.eps')
plt.ion()
plt.show(block=False)
plt.pause(0.01)
plt.close()
#plot fine-grained histogram and adding gaussian fit M(i) old
best_fit_line_old_fg = scipy.stats.norm.pdf(bins_fg, np.mean(Mi_old), np.std(Mi_old))
plt.hist(Mi_old, bins_fg, density=1, alpha=0.5)
plt.plot(bins_fg, best_fit_line_old_fg)
plt.xlabel('M(i) old')
plt.savefig('./PLOTS/' + workingtitle + '/histo_gauss_fine_mi_old_' + workingtitle +  add_out + '.png')
plt.savefig('./PLOTS/' + workingtitle + '/histo_gauss_fine_mi_old_' + workingtitle +  add_out + '.eps')
plt.ion()
plt.show(block=False)
plt.pause(0.01)
plt.close()
#plot fine-grained histogram and adding gaussian fit M(i) new
best_fit_line_new_fg = scipy.stats.norm.pdf(bins_fg, np.mean(Mi_new), np.std(Mi_new))
plt.hist(Mi_new, bins_fg, density=1, alpha=0.5)
plt.plot(bins_fg, best_fit_line_new_fg)
plt.xlabel('M(i) new')
plt.savefig('./PLOTS/' + workingtitle + '/histo_gauss_fine_mi_new_' + workingtitle +  add_out + '.png')
plt.savefig('./PLOTS/' + workingtitle + '/histo_gauss_fine_mi_new_' + workingtitle +  add_out + '.eps')
plt.ion()
plt.show(block=False)
plt.pause(0.01)
plt.close()
#########
#REgion_A
#########
plt.hist(Mi_old_A, bins, color="orange")
plt.xlabel('M(i) old')
plt.savefig('./PLOTS/' + workingtitle + '/histo_mi_old_A_' + workingtitle +  add_out + '.png')  # , dpi=150)
plt.savefig('./PLOTS/' + workingtitle + '/histo_mi_old_A_' + workingtitle +  add_out + '.eps')  # , dpi=150)
plt.ion()
plt.show(block=False)
plt.pause(0.01)
plt.close()
#plot regular histogram M(i) new
plt.hist(Mi_new_A, bins, color="orange")
plt.xlabel('M(i) new')
plt.savefig('./PLOTS/' + workingtitle + '/histo_mi_new_A_' + workingtitle +  add_out + '.png')
plt.savefig('./PLOTS/' + workingtitle + '/histo_mi_new_A_' + workingtitle +  add_out + '.eps')  # , dpi=150)
plt.ion()
plt.show(block=False)
plt.pause(0.01)
plt.close()
#plot fine-grained histogram M(i) old
plt.hist(Mi_old_A, bins_fg, color="orange")
plt.xlabel('M(i) old')
plt.savefig('./PLOTS/' + workingtitle + '/histo_fine_mi_old_A_' + workingtitle +  add_out + '.png')  # , dpi=150)
plt.savefig('./PLOTS/' + workingtitle + '/histo_fine_mi_old_A_' + workingtitle +  add_out + '.eps')  # , dpi=150)
plt.ion()
plt.show(block=False)
plt.pause(0.01)
plt.close()
#plot fine-grained histogram M(i) new
plt.hist(Mi_new_A, bins_fg, color="orange")
plt.xlabel('M(i) new')
plt.savefig('./PLOTS/' + workingtitle + '/histo_fine_mi_new_A_' + workingtitle +  add_out + '.png')
plt.savefig('./PLOTS/' + workingtitle + '/histo_fine_mi_new_A_' + workingtitle +  add_out + '.eps')
plt.ion()
plt.show(block=False)
plt.pause(0.01)
plt.close()
#plot regular histogram and adding gaussian fit M(i) old
best_fit_line_old = scipy.stats.norm.pdf(bins, np.mean(Mi_old), np.std(Mi_old))
plt.hist(Mi_old_A, bins, density=1, alpha=0.5)
plt.plot(bins, best_fit_line_old)
plt.xlabel('M(i) old')
plt.savefig('./PLOTS/' + workingtitle + '/histo_gauss_mi_old_A_' + workingtitle +  add_out + '.png')
plt.savefig('./PLOTS/' + workingtitle + '/histo_gauss_mi_old_A_' + workingtitle +  add_out + '.eps')
plt.ion()
plt.show(block=False)
plt.pause(0.01)
plt.close()
#plot regular histogram and adding gaussian fit M(i) new
best_fit_line_new = scipy.stats.norm.pdf(bins, np.mean(Mi_new), np.std(Mi_new))
plt.hist(Mi_new_A, bins, density=1, alpha=0.5)
plt.plot(bins, best_fit_line_new)
plt.xlabel('M(i) new')
plt.savefig('./PLOTS/' + workingtitle + '/histo_gauss_mi_new_A_' + workingtitle +  add_out + '.png')
plt.savefig('./PLOTS/' + workingtitle + '/histo_gauss_mi_new_A_' + workingtitle +  add_out + '.eps')
plt.ion()
plt.show(block=False)
plt.pause(0.01)
plt.close()
#plot fine-grained histogram and adding gaussian fit M(i) old
best_fit_line_old_fg = scipy.stats.norm.pdf(bins_fg, np.mean(Mi_old), np.std(Mi_old))
plt.hist(Mi_old_A, bins_fg, density=1, alpha=0.5)
plt.plot(bins_fg, best_fit_line_old_fg)
plt.xlabel('M(i) old')
plt.savefig('./PLOTS/' + workingtitle + '/histo_gauss_fine_mi_old_A_' + workingtitle +  add_out + '.png')
plt.savefig('./PLOTS/' + workingtitle + '/histo_gauss_fine_mi_old_A_' + workingtitle +  add_out + '.eps')
plt.ion()
plt.show(block=False)
plt.pause(0.01)
plt.close()
#plot fine-grained histogram and adding gaussian fit M(i) new
best_fit_line_new_fg = scipy.stats.norm.pdf(bins_fg, np.mean(Mi_new), np.std(Mi_new))
plt.hist(Mi_new_A, bins_fg, density=1, alpha=0.5)
plt.plot(bins_fg, best_fit_line_new_fg)
plt.xlabel('M(i) new')
plt.savefig('./PLOTS/' + workingtitle + '/histo_gauss_fine_mi_new_A_' + workingtitle +  add_out + '.png')
plt.savefig('./PLOTS/' + workingtitle + '/histo_gauss_fine_mi_new_A_' + workingtitle +  add_out + '.eps')
plt.ion()
plt.show(block=False)
plt.pause(0.01)
plt.close()
############
#End_A
########
#########
#REgion_H
#########
plt.hist(Mi_old_H, bins, color="orange")
plt.xlabel('M(i) old')
plt.savefig('./PLOTS/' + workingtitle + '/histo_mi_old_H_' + workingtitle +  add_out + '.png')  # , dpi=150)
plt.savefig('./PLOTS/' + workingtitle + '/histo_mi_old_H_' + workingtitle +  add_out + '.eps')  # , dpi=150)
plt.ion()
plt.show(block=False)
plt.pause(0.01)
plt.close()
#plot regular histogram M(i) new
plt.hist(Mi_new_H, bins, color="orange")
plt.xlabel('M(i) new')
plt.savefig('./PLOTS/' + workingtitle + '/histo_mi_new_H_' + workingtitle +  add_out + '.png')
plt.savefig('./PLOTS/' + workingtitle + '/histo_mi_new_H_' + workingtitle +  add_out + '.eps')  # , dpi=150)
plt.ion()
plt.show(block=False)
plt.pause(0.01)
plt.close()
#plot fine-grained histogram M(i) old
plt.hist(Mi_old_H, bins_fg, color="orange")
plt.xlabel('M(i) old')
plt.savefig('./PLOTS/' + workingtitle + '/histo_fine_mi_old_H_' + workingtitle +  add_out + '.png')  # , dpi=150)
plt.savefig('./PLOTS/' + workingtitle + '/histo_fine_mi_old_H_' + workingtitle +  add_out + '.eps')  # , dpi=150)
plt.ion()
plt.show(block=False)
plt.pause(0.01)
plt.close()
#plot fine-grained histogram M(i) new
plt.hist(Mi_new_H, bins_fg, color="orange")
plt.xlabel('M(i) new')
plt.savefig('./PLOTS/' + workingtitle + '/histo_fine_mi_new_H_' + workingtitle +  add_out + '.png')
plt.savefig('./PLOTS/' + workingtitle + '/histo_fine_mi_new_H_' + workingtitle +  add_out + '.eps')
plt.ion()
plt.show(block=False)
plt.pause(0.01)
plt.close()
#plot regular histogram and adding gaussian fit M(i) old
best_fit_line_old = scipy.stats.norm.pdf(bins, np.mean(Mi_old), np.std(Mi_old))
plt.hist(Mi_old_H, bins, density=1, alpha=0.5)
plt.plot(bins, best_fit_line_old)
plt.xlabel('M(i) old')
plt.savefig('./PLOTS/' + workingtitle + '/histo_gauss_mi_old_H_' + workingtitle +  add_out + '.png')
plt.savefig('./PLOTS/' + workingtitle + '/histo_gauss_mi_old_H_' + workingtitle +  add_out + '.eps')
plt.ion()
plt.show(block=False)
plt.pause(0.01)
plt.close()
#plot regular histogram and adding gaussian fit M(i) new
best_fit_line_new = scipy.stats.norm.pdf(bins, np.mean(Mi_new), np.std(Mi_new))
plt.hist(Mi_new_H, bins, density=1, alpha=0.5)
plt.plot(bins, best_fit_line_new)
plt.xlabel('M(i) new')
plt.savefig('./PLOTS/' + workingtitle + '/histo_gauss_mi_new_H_' + workingtitle + add_out + '.png')
plt.savefig('./PLOTS/' + workingtitle + '/histo_gauss_mi_new_H_' + workingtitle + add_out + '.eps')
plt.ion()
plt.show(block=False)
plt.pause(0.01)
plt.close()
#plot fine-grained histogram and adding gaussian fit M(i) old
best_fit_line_old_fg = scipy.stats.norm.pdf(bins_fg, np.mean(Mi_old), np.std(Mi_old))
plt.hist(Mi_old_H, bins_fg, density=1, alpha=0.5)
plt.plot(bins_fg, best_fit_line_old_fg)
plt.xlabel('M(i) old')
plt.savefig('./PLOTS/' + workingtitle + '/histo_gauss_fine_mi_old_H_' + workingtitle + add_out + '.png')
plt.savefig('./PLOTS/' + workingtitle + '/histo_gauss_fine_mi_old_H_' + workingtitle + add_out + '.eps')
plt.ion()
plt.show(block=False)
plt.pause(0.01)
plt.close()
#plot fine-grained histogram and adding gaussian fit M(i) new
best_fit_line_new_fg = scipy.stats.norm.pdf(bins_fg, np.mean(Mi_new), np.std(Mi_new))
plt.hist(Mi_new_H, bins_fg, density=1, alpha=0.5)
plt.plot(bins_fg, best_fit_line_new_fg)
plt.xlabel('M(i) new')
plt.savefig('./PLOTS/' + workingtitle + '/histo_gauss_fine_mi_new_H_' + workingtitle + add_out + '.png')
plt.savefig('./PLOTS/' + workingtitle + '/histo_gauss_fine_mi_new_H_' + workingtitle + add_out + '.eps')
plt.ion()
plt.show(block=False)
plt.pause(0.01)
plt.close()
############
#End_H
########
#########
#REgion_S
#########
plt.hist(Mi_old_S, bins, color="orange")
plt.xlabel('M(i) old')
plt.savefig('./PLOTS/' + workingtitle + '/histo_mi_old_S_' + workingtitle + add_out + '.png')  # , dpi=150)
plt.savefig('./PLOTS/' + workingtitle + '/histo_mi_old_S_' + workingtitle + add_out + '.eps')  # , dpi=150)
plt.ion()
plt.show(block=False)
plt.pause(0.01)
plt.close()
#plot regular histogram M(i) new
plt.hist(Mi_new_S, bins, color="orange")
plt.xlabel('M(i) new')
plt.savefig('./PLOTS/' + workingtitle + '/histo_mi_new_S_' + workingtitle + add_out + '.png')
plt.savefig('./PLOTS/' + workingtitle + '/histo_mi_new_S_' + workingtitle + add_out + '.eps')  # , dpi=150)
plt.ion()
plt.show(block=False)
plt.pause(0.01)
plt.close()
#plot fine-grained histogram M(i) old
plt.hist(Mi_old_S, bins_fg, color="orange")
plt.xlabel('M(i) old')
plt.savefig('./PLOTS/' + workingtitle + '/histo_fine_mi_old_S_' + workingtitle + add_out + '.png')  # , dpi=150)
plt.savefig('./PLOTS/' + workingtitle + '/histo_fine_mi_old_S_' + workingtitle + add_out + '.eps')  # , dpi=150)
plt.ion()
plt.show(block=False)
plt.pause(0.01)
plt.close()
#plot fine-grained histogram M(i) new
plt.hist(Mi_new_S, bins_fg, color="orange")
plt.xlabel('M(i) new')
plt.savefig('./PLOTS/' + workingtitle + '/histo_fine_mi_new_S_' + workingtitle + add_out + '.png')
plt.savefig('./PLOTS/' + workingtitle + '/histo_fine_mi_new_S_' + workingtitle + add_out + '.eps')
plt.ion()
plt.show(block=False)
plt.pause(0.01)
plt.close()
#plot regular histogram and adding gaussian fit M(i) old
best_fit_line_old = scipy.stats.norm.pdf(bins, np.mean(Mi_old), np.std(Mi_old))
plt.hist(Mi_old_S, bins, density=1, alpha=0.5)
plt.plot(bins, best_fit_line_old)
plt.xlabel('M(i) old')
plt.savefig('./PLOTS/' + workingtitle + '/histo_gauss_mi_old_S_' + workingtitle + add_out + '.png')
plt.savefig('./PLOTS/' + workingtitle + '/histo_gauss_mi_old_S_' + workingtitle + add_out + '.eps')
plt.ion()
plt.show(block=False)
plt.pause(0.01)
plt.close()
#plot regular histogram and adding gaussian fit M(i) new
best_fit_line_new = scipy.stats.norm.pdf(bins, np.mean(Mi_new), np.std(Mi_new))
plt.hist(Mi_new_S, bins, density=1, alpha=0.5)
plt.plot(bins, best_fit_line_new)
plt.xlabel('M(i) new')
plt.savefig('./PLOTS/' + workingtitle + '/histo_gauss_mi_new_S_' + workingtitle + add_out + '.png')
plt.savefig('./PLOTS/' + workingtitle + '/histo_gauss_mi_new_S_' + workingtitle + add_out + '.eps')
plt.ion()
plt.show(block=False)
plt.pause(0.01)
plt.close()
#plot fine-grained histogram and adding gaussian fit M(i) old
best_fit_line_old_fg = scipy.stats.norm.pdf(bins_fg, np.mean(Mi_old), np.std(Mi_old))
plt.hist(Mi_old_S, bins_fg, density=1, alpha=0.5)
plt.plot(bins_fg, best_fit_line_old_fg)
plt.xlabel('M(i) old')
plt.savefig('./PLOTS/' + workingtitle + '/histo_gauss_fine_mi_old_S_' + workingtitle + add_out + '.png')
plt.savefig('./PLOTS/' + workingtitle + '/histo_gauss_fine_mi_old_S_' + workingtitle + add_out + '.eps')
plt.ion()
plt.show(block=False)
plt.pause(0.01)
plt.close()
#plot fine-grained histogram and adding gaussian fit M(i) new
best_fit_line_new_fg = scipy.stats.norm.pdf(bins_fg, np.mean(Mi_new), np.std(Mi_new))
plt.hist(Mi_new_S, bins_fg, density=1, alpha=0.5)
plt.plot(bins_fg, best_fit_line_new_fg)
plt.xlabel('M(i) new')
plt.savefig('./PLOTS/' + workingtitle + '/histo_gauss_fine_mi_new_S_' + workingtitle + add_out + '.png')
plt.savefig('./PLOTS/' + workingtitle + '/histo_gauss_fine_mi_new_S_' + workingtitle + add_out + '.eps')
plt.ion()
plt.show(block=False)
plt.pause(0.01)
plt.close()
############
#End_S
########


########################################################################################################################
## END ## PLOT HISTOGRAMS ##############################################################################################
########################################################################################################################
#
#
#
#
#
#
#
#
#
#
########################################################################################################################
# START # PLOT SINGLE VARIABLES ########################################################################################
########################################################################################################################

#year_aux_list.append(i)
#year_aux_list.append(np.mean(MowingMachine_aux_list))
#year_aux_list.append(np.std(MowingMachine_aux_list))
#year_aux_list.append(np.mean(MowingConditioner_aux_list))
#year_aux_list.append(np.std(MowingConditioner_aux_list))
#year_aux_list.append(np.mean(CutWidth_aux_list))
#year_aux_list.append(np.std(CutWidth_aux_list))
#year_aux_list.append(np.mean(CutHeight_aux_list))
#year_aux_list.append(np.std(CutHeight_aux_list))



annual_vars_all = annual_trend_single_variable(data_clean)
annual_vars_all_A = annual_trend_single_variable(data_clean, first_letter="A")
annual_vars_all_S = annual_trend_single_variable(data_clean, first_letter="S")
annual_vars_all_H = annual_trend_single_variable(data_clean, first_letter="H")
#print(annual_vars_all_H)
#print(annual_vars_all_S)
#print(annual_vars_all_A)
#sys.exit()
np.savetxt('./DATA/' + workingtitle + '/annual_trend_single_all_vars' + '_' + workingtitle + add_out + '.csv', annual_vars_all, fmt='%s', delimiter=",")
np.savetxt('./DATA/' + workingtitle + '/annual_trend_single_all_vars_A' + '_' + workingtitle + add_out + '.csv', annual_vars_all_A, fmt='%s', delimiter=",")
np.savetxt('./DATA/' + workingtitle + '/annual_trend_single_all_vars_S' + '_' + workingtitle + add_out + '.csv', annual_vars_all_S, fmt='%s', delimiter=",")
np.savetxt('./DATA/' + workingtitle + '/annual_trend_single_all_vars_H' + '_' + workingtitle + add_out + '.csv', annual_vars_all_H, fmt='%s', delimiter=",")






#MowingMachine
#calculate correlation coefficients
rho_MowingMachine = np.corrcoef(x=annual_vars_all[:,0], y=annual_vars_all[:,1])
#plot annual trends, no errorbars
plt.plot(annual_vars_all[:,0], annual_vars_all[:,1], '-', color="red", label=("Average MowingMachine\nρ=" + str(round(rho_MowingMachine[0,1],3))))
plt.legend(bbox_to_anchor=(0.2, 0.3), loc='upper left', borderaxespad=0.)
#plt.vlines(train_size - 1, 0, max(dataset) * 1.1, colors='purple', linewidth=0.7, linestyles='--')
plt.xlabel('Years')
plt.ylabel('Average MowingMachine')
plt.savefig('./PLOTS/' + workingtitle + '/annual_MowingMachine' + '_' + workingtitle + add_out + '.png')  # , dpi=150)
plt.savefig('./PLOTS/' + workingtitle + '/annual_MowingMachine' + '_' + workingtitle + add_out + '.eps')  # , dpi=150)
plt.ion()
plt.show(block=False)
plt.pause(0.01)
plt.close()
#plot annual trends, with errorbars
plt.errorbar(annual_vars_all[:,0], annual_vars_all[:,1], yerr=annual_vars_all[:,2], color="red", elinewidth=3.0, label=("Average MowingMachine\nρ=" + str(round(rho_MowingMachine[0,1],3))))
plt.legend(bbox_to_anchor=(0.2, 0.3), loc='upper left', borderaxespad=0.)
#plt.vlines(train_size - 1, 0, max(dataset) * 1.1, colors='purple', linewidth=0.7, linestyles='--')
plt.xlabel('Years')
plt.ylabel('Average MowingMachine')
plt.savefig('./PLOTS/' + workingtitle + '/annual_MowingMachine_err' + '_' + workingtitle + add_out + '.png')  # , dpi=150)
plt.savefig('./PLOTS/' + workingtitle + '/annual_MowingMachine_err' + '_' + workingtitle + add_out + '.eps')  # , dpi=150)
plt.ion()
plt.show(block=False)
plt.pause(0.01)
plt.close()


#conditioner

#calculate correlation coefficients
rho_MowingConditioner = np.corrcoef(x=annual_vars_all[:,0], y=annual_vars_all[:,3])
#plot annual trends, no errorbars
plt.plot(annual_vars_all[:,0], annual_vars_all[:,3], '-', color="red", label=("Average MowingConditioner\nρ=" + str(round(rho_MowingConditioner[0,1],3))))
plt.legend(bbox_to_anchor=(0.5, 0.95), loc='upper left', borderaxespad=0.)
#plt.vlines(train_size - 1, 0, max(dataset) * 1.1, colors='purple', linewidth=0.7, linestyles='--')
plt.xlabel('Years')
plt.ylabel('Average MowingConditioner')
plt.savefig('./PLOTS/' + workingtitle + '/annual_MowingConditioner' + '_' + workingtitle + add_out + '.png')  # , dpi=150)
plt.savefig('./PLOTS/' + workingtitle + '/annual_MowingConditioner' + '_' + workingtitle + add_out + '.eps')  # , dpi=150)
plt.ion()
plt.show(block=False)
plt.pause(0.01)
plt.close()
#plot annual trends, with errorbars
plt.errorbar(annual_vars_all[:,0], annual_vars_all[:,3], yerr=annual_vars_all[:,4], color="red", elinewidth=3.0, label=("Average MowingConditioner\nρ=" + str(round(rho_MowingConditioner[0,1],3))))
plt.legend(bbox_to_anchor=(0.5, 0.95), loc='upper left', borderaxespad=0.)
#plt.vlines(train_size - 1, 0, max(dataset) * 1.1, colors='purple', linewidth=0.7, linestyles='--')
plt.xlabel('Years')
plt.ylabel('Average MowingConditioner')
plt.savefig('./PLOTS/' + workingtitle + '/annual_MowingConditioner_err' + '_' + workingtitle + add_out + '.png')  # , dpi=150)
plt.savefig('./PLOTS/' + workingtitle + '/annual_MowingConditioner_err' + '_' + workingtitle + add_out + '.eps')  # , dpi=150)
plt.ion()
plt.show(block=False)
plt.pause(0.01)
plt.close()

#CutWidth

#calculate correlation coefficients
rho_CutWidth = np.corrcoef(x=annual_vars_all[:,0], y=annual_vars_all[:,5])
#plot annual trends, no errorbars
plt.plot(annual_vars_all[:,0], annual_vars_all[:,5], '-', color="red", label=("Average CutWidth\nρ=" + str(round(rho_CutWidth[0,1],3))))
plt.legend(bbox_to_anchor=(0.5, 0.95), loc='upper left', borderaxespad=0.)
#plt.vlines(train_size - 1, 0, max(dataset) * 1.1, colors='purple', linewidth=0.7, linestyles='--')
plt.xlabel('Years')
plt.ylabel('Average CutWidth')
plt.savefig('./PLOTS/' + workingtitle + '/annual_CutWidth' + '_' + workingtitle + add_out + '.png')  # , dpi=150)
plt.savefig('./PLOTS/' + workingtitle + '/annual_CutWidth' + '_' + workingtitle + add_out + '.eps')  # , dpi=150)
plt.ion()
plt.show(block=False)
plt.pause(0.01)
plt.close()
#plot annual trends, with errorbars
plt.errorbar(annual_vars_all[:,0], annual_vars_all[:,5], yerr=annual_vars_all[:,6], color="red", elinewidth=3.0, label=("Average CutWidth\nρ=" + str(round(rho_CutWidth[0,1],3))))
plt.legend(bbox_to_anchor=(0.5, 0.95), loc='upper left', borderaxespad=0.)
#plt.vlines(train_size - 1, 0, max(dataset) * 1.1, colors='purple', linewidth=0.7, linestyles='--')
plt.xlabel('Years')
plt.ylabel('Average CutWidth')
plt.savefig('./PLOTS/' + workingtitle + '/annual_CutWidth_err' + '_' + workingtitle + add_out + '.png')  # , dpi=150)
plt.savefig('./PLOTS/' + workingtitle + '/annual_CutWidth_err' + '_' + workingtitle + add_out + '.eps')  # , dpi=150)
plt.ion()
plt.show(block=False)
plt.pause(0.01)
plt.close()



#CutHeight

#calculate correlation coefficients
rho_CutHeight = np.corrcoef(x=annual_vars_all[:,0], y=annual_vars_all[:,7])
#plot annual trends, no errorbars
plt.plot(annual_vars_all[:,0], annual_vars_all[:,7], '-', color="red", label=("Average CutHeight\nρ=" + str(round(rho_CutHeight[0,1],3))))
plt.legend(bbox_to_anchor=(0.5, 0.95), loc='upper left', borderaxespad=0.)
#plt.vlines(train_size - 1, 0, max(dataset) * 1.1, colors='purple', linewidth=0.7, linestyles='--')
plt.xlabel('Years')
plt.ylabel('Average CutHeight')
plt.savefig('./PLOTS/' + workingtitle + '/annual_CutHeight' + '_' + workingtitle + add_out + '.png')  # , dpi=150)
plt.savefig('./PLOTS/' + workingtitle + '/annual_CutHeight' + '_' + workingtitle + add_out + '.eps')  # , dpi=150)
plt.ion()
plt.show(block=False)
plt.pause(0.01)
plt.close()
#plot annual trends, with errorbars
plt.errorbar(annual_vars_all[:,0], annual_vars_all[:,7], yerr=annual_vars_all[:,8], color="red", elinewidth=3.0, label=("Average CutHeight\nρ=" + str(round(rho_CutHeight[0,1],3))))
plt.legend(bbox_to_anchor=(0.5, 0.95), loc='upper left', borderaxespad=0.)
#plt.vlines(train_size - 1, 0, max(dataset) * 1.1, colors='purple', linewidth=0.7, linestyles='--')
plt.xlabel('Years')
plt.ylabel('Average CutHeight')
plt.savefig('./PLOTS/' + workingtitle + '/annual_CutHeight_err' + '_' + workingtitle + add_out + '.png')  # , dpi=150)
plt.savefig('./PLOTS/' + workingtitle + '/annual_CutHeight_err' + '_' + workingtitle + add_out + '.eps')  # , dpi=150)
plt.ion()
plt.show(block=False)
plt.pause(0.01)
plt.close()












#MowingMachine
#calculate correlation coefficients
rho_MowingMachine_A = np.corrcoef(x=annual_vars_all_A[:,0], y=annual_vars_all_A[:,1])
print(annual_vars_all_A)
print('errors here A')
print(rho_MowingMachine_A)
#plot annual trends, no errorbars
plt.plot(annual_vars_all_A[:,0], annual_vars_all_A[:,1], '-', color="red", label=("Average MowingMachine\nρ=" + str(round(rho_MowingMachine_A[0,1],3))))
plt.legend(bbox_to_anchor=(0.2, 0.3), loc='upper left', borderaxespad=0.)
#plt.vlines(train_size - 1, 0, max(dataset) * 1.1, colors='purple', linewidth=0.7, linestyles='--')
plt.xlabel('Years')
plt.ylabel('Average MowingMachine')
plt.savefig('./PLOTS/' + workingtitle + '/annual_MowingMachine_A' + '_' + workingtitle + add_out + '.png')  # , dpi=150)
plt.savefig('./PLOTS/' + workingtitle + '/annual_MowingMachine_A' + '_' + workingtitle + add_out + '.eps')  # , dpi=150)
plt.ion()
plt.show(block=False)
plt.pause(0.01)
plt.close()
#plot annual trends, with errorbars
plt.errorbar(annual_vars_all_A[:,0], annual_vars_all_A[:,1], yerr=annual_vars_all_A[:,2], color="red", elinewidth=3.0, label=("Average MowingMachine\nρ=" + str(round(rho_MowingMachine_A[0,1],3))))
plt.legend(bbox_to_anchor=(0.2, 0.3), loc='upper left', borderaxespad=0.)
#plt.vlines(train_size - 1, 0, max(dataset) * 1.1, colors='purple', linewidth=0.7, linestyles='--')
plt.xlabel('Years')
plt.ylabel('Average MowingMachine')
plt.savefig('./PLOTS/' + workingtitle + '/annual_MowingMachine_A_err' + '_' + workingtitle + add_out + '.png')  # , dpi=150)
plt.savefig('./PLOTS/' + workingtitle + '/annual_MowingMachine_A_err' + '_' + workingtitle + add_out + '.eps')  # , dpi=150)
plt.ion()
plt.show(block=False)
plt.pause(0.01)
plt.close()


#conditioner

#calculate correlation coefficients
rho_MowingConditioner_A = np.corrcoef(x=annual_vars_all_A[:,0], y=annual_vars_all_A[:,3])
print('errors here A')
print(rho_MowingConditioner_A)
#plot annual trends, no errorbars
plt.plot(annual_vars_all_A[:,0], annual_vars_all_A[:,3], '-', color="red", label=("Average MowingConditioner\nρ=" + str(round(rho_MowingConditioner_A[0,1],3))))
plt.legend(bbox_to_anchor=(0.5, 0.95), loc='upper left', borderaxespad=0.)
#plt.vlines(train_size - 1, 0, max(dataset) * 1.1, colors='purple', linewidth=0.7, linestyles='--')
plt.xlabel('Years')
plt.ylabel('Average MowingConditioner')
plt.savefig('./PLOTS/' + workingtitle + '/annual_MowingConditioner_A' + '_' + workingtitle + add_out + '.png')  # , dpi=150)
plt.savefig('./PLOTS/' + workingtitle + '/annual_MowingConditioner_A' + '_' + workingtitle + add_out + '.eps')  # , dpi=150)
plt.ion()
plt.show(block=False)
plt.pause(0.01)
plt.close()
#plot annual trends, with errorbars
plt.errorbar(annual_vars_all_A[:,0], annual_vars_all_A[:,3], yerr=annual_vars_all_A[:,4], color="red", elinewidth=3.0, label=("Average MowingConditioner\nρ=" + str(round(rho_MowingConditioner_A[0,1],3))))
plt.legend(bbox_to_anchor=(0.5, 0.95), loc='upper left', borderaxespad=0.)
#plt.vlines(train_size - 1, 0, max(dataset) * 1.1, colors='purple', linewidth=0.7, linestyles='--')
plt.xlabel('Years')
plt.ylabel('Average MowingConditioner')
plt.savefig('./PLOTS/' + workingtitle + '/annual_MowingConditioner_A_err' + '_' + workingtitle + add_out + '.png')  # , dpi=150)
plt.savefig('./PLOTS/' + workingtitle + '/annual_MowingConditioner_A_err' + '_' + workingtitle + add_out + '.eps')  # , dpi=150)
plt.ion()
plt.show(block=False)
plt.pause(0.01)
plt.close()

#CutWidth

#calculate correlation coefficients
rho_CutWidth_A = np.corrcoef(x=annual_vars_all_A[:,0], y=annual_vars_all_A[:,5])
print('errors here A')
print(rho_CutWidth_A)
#plot annual trends, no errorbars
plt.plot(annual_vars_all_A[:,0], annual_vars_all_A[:,5], '-', color="red", label=("Average CutWidth\nρ=" + str(round(rho_CutWidth_A[0,1],3))))
plt.legend(bbox_to_anchor=(0.5, 0.95), loc='upper left', borderaxespad=0.)
#plt.vlines(train_size - 1, 0, max(dataset) * 1.1, colors='purple', linewidth=0.7, linestyles='--')
plt.xlabel('Years')
plt.ylabel('Average CutWidth')
plt.savefig('./PLOTS/' + workingtitle + '/annual_CutWidth_A' + '_' + workingtitle + add_out + '.png')  # , dpi=150)
plt.savefig('./PLOTS/' + workingtitle + '/annual_CutWidth_A' + '_' + workingtitle + add_out + '.eps')  # , dpi=150)
plt.ion()
plt.show(block=False)
plt.pause(0.01)
plt.close()
#plot annual trends, with errorbars
plt.errorbar(annual_vars_all_A[:,0], annual_vars_all_A[:,5], yerr=annual_vars_all_A[:,6], color="red", elinewidth=3.0, label=("Average CutWidth\nρ=" + str(round(rho_CutWidth_A[0,1],3))))
plt.legend(bbox_to_anchor=(0.5, 0.95), loc='upper left', borderaxespad=0.)
#plt.vlines(train_size - 1, 0, max(dataset) * 1.1, colors='purple', linewidth=0.7, linestyles='--')
plt.xlabel('Years')
plt.ylabel('Average CutWidth')
plt.savefig('./PLOTS/' + workingtitle + '/annual_CutWidth_A_err' + '_' + workingtitle + add_out + '.png')  # , dpi=150)
plt.savefig('./PLOTS/' + workingtitle + '/annual_CutWidth_A_err' + '_' + workingtitle + add_out + '.eps')  # , dpi=150)
plt.ion()
plt.show(block=False)
plt.pause(0.01)
plt.close()



#CutHeight

#calculate correlation coefficients
rho_CutHeight_A = np.corrcoef(x=annual_vars_all_A[:,0], y=annual_vars_all_A[:,7])
print('errors here A')
print(rho_CutHeight_A)
#plot annual trends, no errorbars
plt.plot(annual_vars_all_A[:,0], annual_vars_all_A[:,7], '-', color="red", label=("Average CutHeight\nρ=" + str(round(rho_CutHeight_A[0,1],3))))
plt.legend(bbox_to_anchor=(0.5, 0.95), loc='upper left', borderaxespad=0.)
#plt.vlines(train_size - 1, 0, max(dataset) * 1.1, colors='purple', linewidth=0.7, linestyles='--')
plt.xlabel('Years')
plt.ylabel('Average CutHeight')
plt.savefig('./PLOTS/' + workingtitle + '/annual_CutHeight_A' + '_' + workingtitle + add_out + '.png')  # , dpi=150)
plt.savefig('./PLOTS/' + workingtitle + '/annual_CutHeight_A' + '_' + workingtitle + add_out + '.eps')  # , dpi=150)
plt.ion()
plt.show(block=False)
plt.pause(0.01)
plt.close()
#plot annual trends, with errorbars
plt.errorbar(annual_vars_all_A[:,0], annual_vars_all_A[:,7], yerr=annual_vars_all_A[:,8], color="red", elinewidth=3.0, label=("Average CutHeight\nρ=" + str(round(rho_CutHeight_A[0,1],3))))
plt.legend(bbox_to_anchor=(0.5, 0.95), loc='upper left', borderaxespad=0.)
#plt.vlines(train_size - 1, 0, max(dataset) * 1.1, colors='purple', linewidth=0.7, linestyles='--')
plt.xlabel('Years')
plt.ylabel('Average CutHeight')
plt.savefig('./PLOTS/' + workingtitle + '/annual_CutHeight_A_err' + '_' + workingtitle + add_out + '.png')  # , dpi=150)
plt.savefig('./PLOTS/' + workingtitle + '/annual_CutHeight_A_err' + '_' + workingtitle + add_out + '.eps')  # , dpi=150)
plt.ion()
plt.show(block=False)
plt.pause(0.01)
plt.close()




#MowingMachine
#calculate correlation coefficients
rho_MowingMachine_S = np.corrcoef(x=annual_vars_all_S[:,0], y=annual_vars_all_S[:,1])
#plot annual trends, no errorbars
plt.plot(annual_vars_all_S[:,0], annual_vars_all_S[:,1], '-', color="red", label=("Average MowingMachine\nρ=" + str(round(rho_MowingMachine_S[0,1],3))))
plt.legend(bbox_to_anchor=(0.2, 0.3), loc='upper left', borderaxespad=0.)
#plt.vlines(train_size - 1, 0, max(dataset) * 1.1, colors='purple', linewidth=0.7, linestyles='--')
plt.xlabel('Years')
plt.ylabel('Average MowingMachine')
plt.savefig('./PLOTS/' + workingtitle + '/annual_MowingMachine_S' + '_' + workingtitle + add_out + '.png')  # , dpi=150)
plt.savefig('./PLOTS/' + workingtitle + '/annual_MowingMachine_S' + '_' + workingtitle + add_out + '.eps')  # , dpi=150)
plt.ion()
plt.show(block=False)
plt.pause(0.01)
plt.close()
#plot annual trends, with errorbars
plt.errorbar(annual_vars_all_S[:,0], annual_vars_all_S[:,1], yerr=annual_vars_all_S[:,2], color="red", elinewidth=3.0, label=("Average MowingMachine\nρ=" + str(round(rho_MowingMachine_S[0,1],3))))
plt.legend(bbox_to_anchor=(0.2, 0.3), loc='upper left', borderaxespad=0.)
#plt.vlines(train_size - 1, 0, max(dataset) * 1.1, colors='purple', linewidth=0.7, linestyles='--')
plt.xlabel('Years')
plt.ylabel('Average MowingMachine')
plt.savefig('./PLOTS/' + workingtitle + '/annual_MowingMachine_S_err' + '_' + workingtitle + add_out + '.png')  # , dpi=150)
plt.savefig('./PLOTS/' + workingtitle + '/annual_MowingMachine_S_err' + '_' + workingtitle + add_out + '.eps')  # , dpi=150)
plt.ion()
plt.show(block=False)
plt.pause(0.01)
plt.close()


#conditioner

#calculate correlation coefficients
rho_MowingConditioner_S = np.corrcoef(x=annual_vars_all_S[:,0], y=annual_vars_all_S[:,3])
#plot annual trends, no errorbars
plt.plot(annual_vars_all_S[:,0], annual_vars_all_S[:,3], '-', color="red", label=("Average MowingConditioner\nρ=" + str(round(rho_MowingConditioner_S[0,1],3))))
plt.legend(bbox_to_anchor=(0.5, 0.95), loc='upper left', borderaxespad=0.)
#plt.vlines(train_size - 1, 0, max(dataset) * 1.1, colors='purple', linewidth=0.7, linestyles='--')
plt.xlabel('Years')
plt.ylabel('Average MowingConditioner')
plt.savefig('./PLOTS/' + workingtitle + '/annual_MowingConditioner_S' + '_' + workingtitle + add_out + '.png')  # , dpi=150)
plt.savefig('./PLOTS/' + workingtitle + '/annual_MowingConditioner_S' + '_' + workingtitle + add_out + '.eps')  # , dpi=150)
plt.ion()
plt.show(block=False)
plt.pause(0.01)
plt.close()
#plot annual trends, with errorbars
plt.errorbar(annual_vars_all_S[:,0], annual_vars_all_S[:,3], yerr=annual_vars_all_S[:,4], color="red", elinewidth=3.0, label=("Average MowingConditioner\nρ=" + str(round(rho_MowingConditioner_S[0,1],3))))
plt.legend(bbox_to_anchor=(0.5, 0.95), loc='upper left', borderaxespad=0.)
#plt.vlines(train_size - 1, 0, max(dataset) * 1.1, colors='purple', linewidth=0.7, linestyles='--')
plt.xlabel('Years')
plt.ylabel('Average MowingConditioner')
plt.savefig('./PLOTS/' + workingtitle + '/annual_MowingConditioner_S_err' + '_' + workingtitle + add_out + '.png')  # , dpi=150)
plt.savefig('./PLOTS/' + workingtitle + '/annual_MowingConditioner_S_err' + '_' + workingtitle + add_out + '.eps')  # , dpi=150)
plt.ion()
plt.show(block=False)
plt.pause(0.01)
plt.close()

#CutWidth

#calculate correlation coefficients
rho_CutWidth_S = np.corrcoef(x=annual_vars_all_S[:,0], y=annual_vars_all_S[:,5])
#plot annual trends, no errorbars
plt.plot(annual_vars_all_S[:,0], annual_vars_all_S[:,5], '-', color="red", label=("Average CutWidth\nρ=" + str(round(rho_CutWidth_S[0,1],3))))
plt.legend(bbox_to_anchor=(0.5, 0.95), loc='upper left', borderaxespad=0.)
#plt.vlines(train_size - 1, 0, max(dataset) * 1.1, colors='purple', linewidth=0.7, linestyles='--')
plt.xlabel('Years')
plt.ylabel('Average CutWidth')
plt.savefig('./PLOTS/' + workingtitle + '/annual_CutWidth_S' + '_' + workingtitle + add_out + '.png')  # , dpi=150)
plt.savefig('./PLOTS/' + workingtitle + '/annual_CutWidth_S' + '_' + workingtitle + add_out + '.eps')  # , dpi=150)
plt.ion()
plt.show(block=False)
plt.pause(0.01)
plt.close()
#plot annual trends, with errorbars
plt.errorbar(annual_vars_all_S[:,0], annual_vars_all_S[:,5], yerr=annual_vars_all_S[:,6], color="red", elinewidth=3.0, label=("Average CutWidth\nρ=" + str(round(rho_CutWidth_S[0,1],3))))
plt.legend(bbox_to_anchor=(0.5, 0.95), loc='upper left', borderaxespad=0.)
#plt.vlines(train_size - 1, 0, max(dataset) * 1.1, colors='purple', linewidth=0.7, linestyles='--')
plt.xlabel('Years')
plt.ylabel('Average CutWidth')
plt.savefig('./PLOTS/' + workingtitle + '/annual_CutWidth_S_err' + '_' + workingtitle + add_out + '.png')  # , dpi=150)
plt.savefig('./PLOTS/' + workingtitle + '/annual_CutWidth_S_err' + '_' + workingtitle + add_out + '.eps')  # , dpi=150)
plt.ion()
plt.show(block=False)
plt.pause(0.01)
plt.close()



#CutHeight

#calculate correlation coefficients
rho_CutHeight_S = np.corrcoef(x=annual_vars_all_S[:,0], y=annual_vars_all_S[:,7])
#plot annual trends, no errorbars
plt.plot(annual_vars_all_S[:,0], annual_vars_all_S[:,7], '-', color="red", label=("Average CutHeight\nρ=" + str(round(rho_CutHeight_S[0,1],3))))
plt.legend(bbox_to_anchor=(0.5, 0.95), loc='upper left', borderaxespad=0.)
#plt.vlines(train_size - 1, 0, max(dataset) * 1.1, colors='purple', linewidth=0.7, linestyles='--')
plt.xlabel('Years')
plt.ylabel('Average CutHeight')
plt.savefig('./PLOTS/' + workingtitle + '/annual_CutHeight_S' + '_' + workingtitle + add_out + '.png')  # , dpi=150)
plt.savefig('./PLOTS/' + workingtitle + '/annual_CutHeight_S' + '_' + workingtitle + add_out + '.eps')  # , dpi=150)
plt.ion()
plt.show(block=False)
plt.pause(0.01)
plt.close()
#plot annual trends, with errorbars
plt.errorbar(annual_vars_all_S[:,0], annual_vars_all_S[:,7], yerr=annual_vars_all_S[:,8], color="red", elinewidth=3.0, label=("Average CutHeight\nρ=" + str(round(rho_CutHeight_S[0,1],3))))
plt.legend(bbox_to_anchor=(0.5, 0.95), loc='upper left', borderaxespad=0.)
#plt.vlines(train_size - 1, 0, max(dataset) * 1.1, colors='purple', linewidth=0.7, linestyles='--')
plt.xlabel('Years')
plt.ylabel('Average CutHeight')
plt.savefig('./PLOTS/' + workingtitle + '/annual_CutHeight_S_err' + '_' + workingtitle + add_out + '.png')  # , dpi=150)
plt.savefig('./PLOTS/' + workingtitle + '/annual_CutHeight_S_err' + '_' + workingtitle + add_out + '.eps')  # , dpi=150)
plt.ion()
plt.show(block=False)
plt.pause(0.01)
plt.close()








#MowingMachine
#calculate correlation coefficients
rho_MowingMachine_H = np.corrcoef(x=annual_vars_all_H[:,0], y=annual_vars_all_H[:,1])
#plot annual trends, no errorbars
plt.plot(annual_vars_all_H[:,0], annual_vars_all_H[:,1], '-', color="red", label=("Average MowingMachine\nρ=" + str(round(rho_MowingMachine_H[0,1],3))))
plt.legend(bbox_to_anchor=(0.2, 0.3), loc='upper left', borderaxespad=0.)
#plt.vlines(train_size - 1, 0, max(dataset) * 1.1, colors='purple', linewidth=0.7, linestyles='--')
plt.xlabel('Years')
plt.ylabel('Average MowingMachine')
plt.savefig('./PLOTS/' + workingtitle + '/annual_MowingMachine_H' + '_' + workingtitle + add_out + '.png')  # , dpi=150)
plt.savefig('./PLOTS/' + workingtitle + '/annual_MowingMachine_H' + '_' + workingtitle + add_out + '.eps')  # , dpi=150)
plt.ion()
plt.show(block=False)
plt.pause(0.01)
plt.close()
#plot annual trends, with errorbars
plt.errorbar(annual_vars_all_H[:,0], annual_vars_all_H[:,1], yerr=annual_vars_all_H[:,2], color="red", elinewidth=3.0, label=("Average MowingMachine\nρ=" + str(round(rho_MowingMachine_H[0,1],3))))
plt.legend(bbox_to_anchor=(0.2, 0.3), loc='upper left', borderaxespad=0.)
#plt.vlines(train_size - 1, 0, max(dataset) * 1.1, colors='purple', linewidth=0.7, linestyles='--')
plt.xlabel('Years')
plt.ylabel('Average MowingMachine')
plt.savefig('./PLOTS/' + workingtitle + '/annual_MowingMachine_H_err' + '_' + workingtitle + add_out + '.png')  # , dpi=150)
plt.savefig('./PLOTS/' + workingtitle + '/annual_MowingMachine_H_err' + '_' + workingtitle + add_out + '.eps')  # , dpi=150)
plt.ion()
plt.show(block=False)
plt.pause(0.01)
plt.close()


#conditioner

#calculate correlation coefficients
rho_MowingConditioner_H = np.corrcoef(x=annual_vars_all_H[:,0], y=annual_vars_all_H[:,3])
#plot annual trends, no errorbars
plt.plot(annual_vars_all_H[:,0], annual_vars_all_H[:,3], '-', color="red", label=("Average MowingConditioner\nρ=" + str(round(rho_MowingConditioner_H[0,1],3))))
plt.legend(bbox_to_anchor=(0.5, 0.95), loc='upper left', borderaxespad=0.)
#plt.vlines(train_size - 1, 0, max(dataset) * 1.1, colors='purple', linewidth=0.7, linestyles='--')
plt.xlabel('Years')
plt.ylabel('Average MowingConditioner')
plt.savefig('./PLOTS/' + workingtitle + '/annual_MowingConditioner_H' + '_' + workingtitle + add_out + '.png')  # , dpi=150)
plt.savefig('./PLOTS/' + workingtitle + '/annual_MowingConditioner_H' + '_' + workingtitle + add_out + '.eps')  # , dpi=150)
plt.ion()
plt.show(block=False)
plt.pause(0.01)
plt.close()
#plot annual trends, with errorbars
plt.errorbar(annual_vars_all_H[:,0], annual_vars_all_H[:,3], yerr=annual_vars_all_H[:,4], color="red", elinewidth=3.0, label=("Average MowingConditioner\nρ=" + str(round(rho_MowingConditioner_H[0,1],3))))
plt.legend(bbox_to_anchor=(0.5, 0.95), loc='upper left', borderaxespad=0.)
#plt.vlines(train_size - 1, 0, max(dataset) * 1.1, colors='purple', linewidth=0.7, linestyles='--')
plt.xlabel('Years')
plt.ylabel('Average MowingConditioner')
plt.savefig('./PLOTS/' + workingtitle + '/annual_MowingConditioner_H_err' + '_' + workingtitle + add_out + '.png')  # , dpi=150)
plt.savefig('./PLOTS/' + workingtitle + '/annual_MowingConditioner_H_err' + '_' + workingtitle + add_out + '.eps')  # , dpi=150)
plt.ion()
plt.show(block=False)
plt.pause(0.01)
plt.close()

#CutWidth

#calculate correlation coefficients
rho_CutWidth_H = np.corrcoef(x=annual_vars_all_H[:,0], y=annual_vars_all_H[:,5])
#plot annual trends, no errorbars
plt.plot(annual_vars_all_H[:,0], annual_vars_all_H[:,5], '-', color="red", label=("Average CutWidth\nρ=" + str(round(rho_CutWidth_H[0,1],3))))
plt.legend(bbox_to_anchor=(0.5, 0.95), loc='upper left', borderaxespad=0.)
#plt.vlines(train_size - 1, 0, max(dataset) * 1.1, colors='purple', linewidth=0.7, linestyles='--')
plt.xlabel('Years')
plt.ylabel('Average CutWidth')
plt.savefig('./PLOTS/' + workingtitle + '/annual_CutWidth_H' + '_' + workingtitle + add_out + '.png')  # , dpi=150)
plt.savefig('./PLOTS/' + workingtitle + '/annual_CutWidth_H' + '_' + workingtitle + add_out + '.eps')  # , dpi=150)
plt.ion()
plt.show(block=False)
plt.pause(0.01)
plt.close()
#plot annual trends, with errorbars
plt.errorbar(annual_vars_all_H[:,0], annual_vars_all_H[:,5], yerr=annual_vars_all_H[:,6], color="red", elinewidth=3.0, label=("Average CutWidth\nρ=" + str(round(rho_CutWidth_H[0,1],3))))
plt.legend(bbox_to_anchor=(0.5, 0.95), loc='upper left', borderaxespad=0.)
#plt.vlines(train_size - 1, 0, max(dataset) * 1.1, colors='purple', linewidth=0.7, linestyles='--')
plt.xlabel('Years')
plt.ylabel('Average CutWidth')
plt.savefig('./PLOTS/' + workingtitle + '/annual_CutWidth_H_err' + '_' + workingtitle + add_out + '.png')  # , dpi=150)
plt.savefig('./PLOTS/' + workingtitle + '/annual_CutWidth_H_err' + '_' + workingtitle + add_out + '.eps')  # , dpi=150)
plt.ion()
plt.show(block=False)
plt.pause(0.01)
plt.close()



#CutHeight

#calculate correlation coefficients
rho_CutHeight_H = np.corrcoef(x=annual_vars_all_H[:,0], y=annual_vars_all_H[:,7])
print('errors here')
print(rho_CutHeight_H)
#plot annual trends, no errorbars
plt.plot(annual_vars_all_H[:,0], annual_vars_all_H[:,7], '-', color="red", label=("Average CutHeight\nρ=" + str(round(rho_CutHeight_H[0,1],3))))
plt.legend(bbox_to_anchor=(0.5, 0.95), loc='upper left', borderaxespad=0.)
#plt.vlines(train_size - 1, 0, max(dataset) * 1.1, colors='purple', linewidth=0.7, linestyles='--')
plt.xlabel('Years')
plt.ylabel('Average CutHeight')
plt.savefig('./PLOTS/' + workingtitle + '/annual_CutHeight_H' + '_' + workingtitle + add_out + '.png')  # , dpi=150)
plt.savefig('./PLOTS/' + workingtitle + '/annual_CutHeight_H' + '_' + workingtitle + add_out + '.eps')  # , dpi=150)
plt.ion()
plt.show(block=False)
plt.pause(0.01)
plt.close()
#plot annual trends, with errorbars
plt.errorbar(annual_vars_all_H[:,0], annual_vars_all_H[:,7], yerr=annual_vars_all_H[:,8], color="red", elinewidth=3.0, label=("Average CutHeight\nρ=" + str(round(rho_CutHeight_H[0,1],3))))
plt.legend(bbox_to_anchor=(0.5, 0.95), loc='upper left', borderaxespad=0.)
#plt.vlines(train_size - 1, 0, max(dataset) * 1.1, colors='purple', linewidth=0.7, linestyles='--')
plt.xlabel('Years')
plt.ylabel('Average CutHeight')
plt.savefig('./PLOTS/' + workingtitle + '/annual_CutHeight_H_err' + '_' + workingtitle + add_out + '.png')  # , dpi=150)
plt.savefig('./PLOTS/' + workingtitle + '/annual_CutHeight_H_err' + '_' + workingtitle + add_out + '.eps')  # , dpi=150)
plt.ion()
plt.show(block=False)
plt.pause(0.01)
plt.close()













########################################################################################################################
## END # PLOT SINGLE VARIABLES #########################################################################################
########################################################################################################################
#
#
#
#
#
#
#
#
#
#
########################################################################################################################
# START # PLOT ANNUAL TRENDS ###########################################################################################
########################################################################################################################
print('Plot annual trends')
#calculate annual trends
print(Mi_new)
annual_trend_single_Mi_new, annual_trend_single_Mi_new_t = dc(annual_trend_single(data_clean, Mi_new))
#print(annual_trend_single_Mi_new)
#print(annual_trend_single_Mi_new_t)
#print('#########')
#print(annual_trend_single_Mi_new[0])
#print(annual_trend_single_Mi_new_t[0])



annual_trend_single_Mi_old, annual_trend_single_Mi_old_t = dc(annual_trend_single(data_clean, Mi_old))

annual_trend_single_Mi_new_A, annual_trend_single_Mi_new_A_t = dc(annual_trend_single(data_clean, Mi_new, first_letter="A"))
annual_trend_single_Mi_old_A, annual_trend_single_Mi_old_A_t = dc(annual_trend_single(data_clean, Mi_old, first_letter="A"))

annual_trend_single_Mi_new_S, annual_trend_single_Mi_new_S_t = dc(annual_trend_single(data_clean, Mi_new, first_letter="S"))
annual_trend_single_Mi_old_S, annual_trend_single_Mi_old_S_t = dc(annual_trend_single(data_clean, Mi_old, first_letter="S"))

annual_trend_single_Mi_new_H, annual_trend_single_Mi_new_H_t = dc(annual_trend_single(data_clean, Mi_new, first_letter="H"))
annual_trend_single_Mi_old_H, annual_trend_single_Mi_old_H_t = dc(annual_trend_single(data_clean, Mi_old, first_letter="H"))




annual_trend_Mi_new = annual_trend(data_clean, Mi_new)
annual_trend_Mi_old = annual_trend(data_clean, Mi_old)
annual_trend_Mi_old_A = annual_trend_regional(data_clean, Mi_old, "A")
annual_trend_Mi_new_A = annual_trend_regional(data_clean, Mi_new, "A")
annual_trend_Mi_old_S = annual_trend_regional(data_clean, Mi_old, "S")
annual_trend_Mi_new_S = annual_trend_regional(data_clean, Mi_new, "S")
annual_trend_Mi_old_H = annual_trend_regional(data_clean, Mi_old, "H")
annual_trend_Mi_new_H = annual_trend_regional(data_clean, Mi_new, "H")

#sys.exit()
#annual_trend_Mi_new_A = annual_trend_regional(data_clean, Mi_new)
#sys.exit()

#save for comparison
np.savetxt('./DATA/' + workingtitle + '/annual_Mi_new' + '_' + workingtitle + add_out + '.csv', annual_trend_Mi_new, fmt='%s', delimiter=",")
np.savetxt('./DATA/' + workingtitle + '/annual_Mi_old.csv', annual_trend_Mi_old, fmt='%s', delimiter=",")
#calculate correlation coefficients
rho_m1_new = np.corrcoef(x=annual_trend_Mi_new[:,0], y=annual_trend_Mi_new[:,1])
rho_m1_old = np.corrcoef(x=annual_trend_Mi_old[:,0], y=annual_trend_Mi_old[:,1])
#plot annual trends, no errorbars
plt.plot(annual_trend_Mi_old[:,0], annual_trend_Mi_old[:,1], '-', color="red", label=("M(i) old\nρ=" + str(round(rho_m1_old[0,1],3))))
plt.plot(annual_trend_Mi_new[:,0], annual_trend_Mi_new[:,1], '-', color="blue", label=("M(i) new\nρ=" + str(round(rho_m1_new[0,1],3))))
plt.legend(bbox_to_anchor=(0.73, 0.95), loc='upper left', borderaxespad=0.)
#plt.vlines(train_size - 1, 0, max(dataset) * 1.1, colors='purple', linewidth=0.7, linestyles='--')
plt.xlabel('Years')
plt.ylabel('Average M(i)')
plt.savefig('./PLOTS/' + workingtitle + '/annual_average_mi' + '_' + workingtitle + add_out + '.png')  # , dpi=150)
plt.savefig('./PLOTS/' + workingtitle + '/annual_average_mi' + '_' + workingtitle + add_out + '.eps')  # , dpi=150)
plt.ion()
plt.show(block=False)
plt.pause(0.01)
plt.close()
#plot annual trends, with errorbars
plt.errorbar(annual_trend_Mi_old[:,0], annual_trend_Mi_old[:,1], yerr=annual_trend_Mi_old[:,2], color="red", elinewidth=3.0, label=("M(i) old\nρ=" + str(round(rho_m1_old[0,1],3))))
plt.errorbar(annual_trend_Mi_new[:,0], annual_trend_Mi_new[:,1], yerr=annual_trend_Mi_new[:,2], color="blue", elinewidth=1.0, label=("M(i) new\nρ=" + str(round(rho_m1_new[0,1],3))))
plt.legend(bbox_to_anchor=(0.73, 0.95), loc='upper left', borderaxespad=0.)
#plt.vlines(train_size - 1, 0, max(dataset) * 1.1, colors='purple', linewidth=0.7, linestyles='--')
plt.xlabel('Years')
plt.ylabel('Average M(i)')
plt.savefig('./PLOTS/' + workingtitle + '/annual_average_mi_err' + '_' + workingtitle + add_out + '.png')  # , dpi=150)
plt.savefig('./PLOTS/' + workingtitle + '/annual_average_mi_err' + '_' + workingtitle + add_out + '.eps')  # , dpi=150)
plt.ion()
plt.show(block=False)
plt.pause(0.01)
plt.close()
#plot annual trends, no errorbars, single paths, new Mi
color1 = iter(cm.rainbow(np.linspace(0.2, 0.6, len(annual_trend_single_Mi_new))))
for i in range(len(annual_trend_single_Mi_new)):
    c1 = next(color1)
    plt.plot(annual_trend_single_Mi_new_t[i], annual_trend_single_Mi_new[i], color=c1)
#plt.plot(annual_trend_Mi_old[:,0], annual_trend_Mi_old[:,1], '-', color="red", label=("M(i) old\nρ=" + str(round(rho_m1_old[0,1],3))))
plt.plot(annual_trend_Mi_new[:,0], annual_trend_Mi_new[:,1], '-', color="red", label=("Average M(i) new\nρ=" + str(round(rho_m1_new[0,1],3))))
plt.legend(bbox_to_anchor=(0.73, 0.95), loc='upper left', borderaxespad=0.)
#plt.vlines(train_size - 1, 0, max(dataset) * 1.1, colors='purple', linewidth=0.7, linestyles='--')
plt.xlabel('Years')
plt.ylabel('M(i) new')
plt.savefig('./PLOTS/' + workingtitle + '/annual_average_mi_single_new' + '_' + workingtitle + add_out + '.png')  # , dpi=150)
plt.savefig('./PLOTS/' + workingtitle + '/annual_average_mi_single_new' + '_' + workingtitle + add_out + '.eps')  # , dpi=150)
plt.ion()
plt.show(block=False)
plt.pause(0.01)
plt.close()
#plot annual trends, no errorbars, single paths, old Mi
color2 = iter(cm.rainbow(np.linspace(0.2, 0.6, len(annual_trend_single_Mi_old))))
for i in range(len(annual_trend_single_Mi_old)):
    c2 = next(color2)
    plt.plot(annual_trend_single_Mi_old_t[i], annual_trend_single_Mi_old[i], color=c2)
#plt.plot(annual_trend_Mi_old[:,0], annual_trend_Mi_old[:,1], '-', color="red", label=("M(i) old\nρ=" + str(round(rho_m1_old[0,1],3))))
plt.plot(annual_trend_Mi_old[:,0], annual_trend_Mi_old[:,1], '-', color="red", label=("Average M(i) old\nρ=" + str(round(rho_m1_old[0,1],3))))
plt.legend(bbox_to_anchor=(0.73, 0.95), loc='upper left', borderaxespad=0.)
#plt.vlines(train_size - 1, 0, max(dataset) * 1.1, colors='purple', linewidth=0.7, linestyles='--')
plt.xlabel('Years')
plt.ylabel('M(i) old')
plt.savefig('./PLOTS/' + workingtitle + '/annual_average_mi_single_old' + '_' + workingtitle + add_out + '.png')  # , dpi=150)
plt.savefig('./PLOTS/' + workingtitle + '/annual_average_mi_single_old' + '_' + workingtitle + add_out + '.eps')  # , dpi=150)
plt.ion()
plt.show(block=False)
plt.pause(0.01)
plt.close()




#calculate correlation coefficients
rho_m1_new_A = np.corrcoef(x=annual_trend_Mi_new_A[:,0], y=annual_trend_Mi_new_A[:,1])
rho_m1_old_A = np.corrcoef(x=annual_trend_Mi_old_A[:,0], y=annual_trend_Mi_old_A[:,1])
#plot annual trends, no errorbars A
plt.plot(annual_trend_Mi_old_A[:,0], annual_trend_Mi_old_A[:,1], '-', color="red", label=("M(i) old\nρ=" + str(round(rho_m1_old_A[0,1],3))))
plt.plot(annual_trend_Mi_new_A[:,0], annual_trend_Mi_new_A[:,1], '-', color="blue", label=("M(i) new\nρ=" + str(round(rho_m1_new_A[0,1],3))))
plt.legend(bbox_to_anchor=(0.73, 0.95), loc='upper left', borderaxespad=0.)
#plt.vlines(train_size - 1, 0, max(dataset) * 1.1, colors='purple', linewidth=0.7, linestyles='--')
plt.xlabel('Years')
plt.ylabel('Average M(i)')
plt.savefig('./PLOTS/' + workingtitle + '/annual_average_mi_A' + '_' + workingtitle + add_out + '.png')  # , dpi=150)
plt.savefig('./PLOTS/' + workingtitle + '/annual_average_mi_A' + '_' + workingtitle + add_out + '.eps')  # , dpi=150)
plt.ion()
plt.show(block=False)
plt.pause(0.01)
plt.close()
#plot annual trends, with errorbars
plt.errorbar(annual_trend_Mi_old_A[:,0], annual_trend_Mi_old_A[:,1], yerr=annual_trend_Mi_old_A[:,2], color="red", elinewidth=3.0, label=("M(i) old\nρ=" + str(round(rho_m1_old_A[0,1],3))))
plt.errorbar(annual_trend_Mi_new_A[:,0], annual_trend_Mi_new_A[:,1], yerr=annual_trend_Mi_new_A[:,2], color="blue", elinewidth=1.0, label=("M(i) new\nρ=" + str(round(rho_m1_new_A[0,1],3))))
plt.legend(bbox_to_anchor=(0.73, 0.95), loc='upper left', borderaxespad=0.)
#plt.vlines(train_size - 1, 0, max(dataset) * 1.1, colors='purple', linewidth=0.7, linestyles='--')
plt.xlabel('Years')
plt.ylabel('Average M(i)')
plt.savefig('./PLOTS/' + workingtitle + '/annual_average_mi_err_A' + '_' + workingtitle + add_out + '.png')  # , dpi=150)
plt.savefig('./PLOTS/' + workingtitle + '/annual_average_mi_err_A' + '_' + workingtitle + add_out + '.eps')  # , dpi=150)
plt.ion()
plt.show(block=False)
plt.pause(0.01)
plt.close()
#plot annual trends, no errorbars, single paths, new Mi
color3 = iter(cm.rainbow(np.linspace(0.2, 0.6, len(annual_trend_single_Mi_new_A))))
for i in range(len(annual_trend_single_Mi_new_A)):
    c3 = next(color3)
    plt.plot(annual_trend_single_Mi_new_A_t[i], annual_trend_single_Mi_new_A[i], color=c3)
#plt.plot(annual_trend_Mi_old[:,0], annual_trend_Mi_old[:,1], '-', color="red", label=("M(i) old\nρ=" + str(round(rho_m1_old[0,1],3))))
plt.plot(annual_trend_Mi_new_A[:,0], annual_trend_Mi_new_A[:,1], '-', color="red", label=("Average M(i) new\nρ=" + str(round(rho_m1_new_A[0,1],3))))
plt.legend(bbox_to_anchor=(0.73, 0.95), loc='upper left', borderaxespad=0.)
#plt.vlines(train_size - 1, 0, max(dataset) * 1.1, colors='purple', linewidth=0.7, linestyles='--')
plt.xlabel('Years')
plt.ylabel('M(i) new')
plt.savefig('./PLOTS/' + workingtitle + '/annual_average_mi_single_new_A' + '_' + workingtitle + add_out + '.png')  # , dpi=150)
plt.savefig('./PLOTS/' + workingtitle + '/annual_average_mi_single_new_A' + '_' + workingtitle + add_out + '.eps')  # , dpi=150)
plt.ion()
plt.show(block=False)
plt.pause(0.01)
plt.close()
#plot annual trends, no errorbars, single paths, old Mi
color4 = iter(cm.rainbow(np.linspace(0.2, 0.6, len(annual_trend_single_Mi_old_A))))
for i in range(len(annual_trend_single_Mi_old_A)):
    c4 = next(color4)
    plt.plot(annual_trend_single_Mi_old_A_t[i], annual_trend_single_Mi_old_A[i], color=c4)
#plt.plot(annual_trend_Mi_old[:,0], annual_trend_Mi_old[:,1], '-', color="red", label=("M(i) old\nρ=" + str(round(rho_m1_old[0,1],3))))
plt.plot(annual_trend_Mi_old_A[:,0], annual_trend_Mi_old_A[:,1], '-', color="red", label=("Average M(i) old\nρ=" + str(round(rho_m1_old_A[0,1],3))))
plt.legend(bbox_to_anchor=(0.73, 0.95), loc='upper left', borderaxespad=0.)
#plt.vlines(train_size - 1, 0, max(dataset) * 1.1, colors='purple', linewidth=0.7, linestyles='--')
plt.xlabel('Years')
plt.ylabel('M(i) old')
plt.savefig('./PLOTS/' + workingtitle + '/annual_average_mi_single_old_A' + '_' + workingtitle + add_out + '.png')  # , dpi=150)
plt.savefig('./PLOTS/' + workingtitle + '/annual_average_mi_single_old_A' + '_' + workingtitle + add_out + '.eps')  # , dpi=150)
plt.ion()
plt.show(block=False)
plt.pause(0.01)
plt.close()


#calculate correlation coefficients
rho_m1_new_S = np.corrcoef(x=annual_trend_Mi_new_S[:,0], y=annual_trend_Mi_new_S[:,1])
rho_m1_old_S = np.corrcoef(x=annual_trend_Mi_old_S[:,0], y=annual_trend_Mi_old_S[:,1])
#plot annual trends, no errorbars A
plt.plot(annual_trend_Mi_old_S[:,0], annual_trend_Mi_old_S[:,1], '-', color="red", label=("M(i) old\nρ=" + str(round(rho_m1_old_S[0,1],3))))
plt.plot(annual_trend_Mi_new_S[:,0], annual_trend_Mi_new_S[:,1], '-', color="blue", label=("M(i) new\nρ=" + str(round(rho_m1_new_S[0,1],3))))
plt.legend(bbox_to_anchor=(0.73, 0.95), loc='upper left', borderaxespad=0.)
#plt.vlines(train_size - 1, 0, max(dataset) * 1.1, colors='purple', linewidth=0.7, linestyles='--')
plt.xlabel('Years')
plt.ylabel('Average M(i)')
plt.savefig('./PLOTS/' + workingtitle + '/annual_average_mi_S' + '_' + workingtitle + add_out + '.png')  # , dpi=150)
plt.savefig('./PLOTS/' + workingtitle + '/annual_average_mi_S' + '_' + workingtitle + add_out + '.eps')  # , dpi=150)
plt.ion()
plt.show(block=False)
plt.pause(0.01)
plt.close()
#plot annual trends, with errorbars
plt.errorbar(annual_trend_Mi_old_S[:,0], annual_trend_Mi_old_S[:,1], yerr=annual_trend_Mi_old_S[:,2], color="red", elinewidth=3.0, label=("M(i) old\nρ=" + str(round(rho_m1_old_S[0,1],3))))
plt.errorbar(annual_trend_Mi_new_S[:,0], annual_trend_Mi_new_S[:,1], yerr=annual_trend_Mi_new_S[:,2], color="blue", elinewidth=1.0, label=("M(i) new\nρ=" + str(round(rho_m1_new_S[0,1],3))))
plt.legend(bbox_to_anchor=(0.73, 0.95), loc='upper left', borderaxespad=0.)
#plt.vlines(train_size - 1, 0, max(dataset) * 1.1, colors='purple', linewidth=0.7, linestyles='--')
plt.xlabel('Years')
plt.ylabel('Average M(i)')
plt.savefig('./PLOTS/' + workingtitle + '/annual_average_mi_err_S' + '_' + workingtitle + add_out + '.png')  # , dpi=150)
plt.savefig('./PLOTS/' + workingtitle + '/annual_average_mi_err_S' + '_' + workingtitle + add_out + '.eps')  # , dpi=150)
plt.ion()
plt.show(block=False)
plt.pause(0.01)
plt.close()
#plot annual trends, no errorbars, single paths, new Mi
color5 = iter(cm.rainbow(np.linspace(0.2, 0.6, len(annual_trend_single_Mi_new_S))))
for i in range(len(annual_trend_single_Mi_new_S)):
    c5 = next(color5)
    plt.plot(annual_trend_single_Mi_new_S_t[i], annual_trend_single_Mi_new_S[i], color=c5)
#plt.plot(annual_trend_Mi_old[:,0], annual_trend_Mi_old[:,1], '-', color="red", label=("M(i) old\nρ=" + str(round(rho_m1_old[0,1],3))))
plt.plot(annual_trend_Mi_new_S[:,0], annual_trend_Mi_new_S[:,1], '-', color="red", label=("Average M(i) new\nρ=" + str(round(rho_m1_new_S[0,1],3))))
plt.legend(bbox_to_anchor=(0.73, 0.95), loc='upper left', borderaxespad=0.)
#plt.vlines(train_size - 1, 0, max(dataset) * 1.1, colors='purple', linewidth=0.7, linestyles='--')
plt.xlabel('Years')
plt.ylabel('M(i) new')
plt.savefig('./PLOTS/' + workingtitle + '/annual_average_mi_single_new_S' + '_' + workingtitle + add_out + '.png')  # , dpi=150)
plt.savefig('./PLOTS/' + workingtitle + '/annual_average_mi_single_new_S' + '_' + workingtitle + add_out + '.eps')  # , dpi=150)
plt.ion()
plt.show(block=False)
plt.pause(0.01)
plt.close()
#plot annual trends, no errorbars, single paths, old Mi
color6 = iter(cm.rainbow(np.linspace(0.2, 0.6, len(annual_trend_single_Mi_old_S))))
for i in range(len(annual_trend_single_Mi_old_S)):
    c6 = next(color6)
    plt.plot(annual_trend_single_Mi_old_S_t[i], annual_trend_single_Mi_old_S[i], color=c6)
#plt.plot(annual_trend_Mi_old[:,0], annual_trend_Mi_old[:,1], '-', color="red", label=("M(i) old\nρ=" + str(round(rho_m1_old[0,1],3))))
plt.plot(annual_trend_Mi_old_S[:,0], annual_trend_Mi_old_S[:,1], '-', color="red", label=("Average M(i) old\nρ=" + str(round(rho_m1_old_S[0,1],3))))
plt.legend(bbox_to_anchor=(0.73, 0.95), loc='upper left', borderaxespad=0.)
#plt.vlines(train_size - 1, 0, max(dataset) * 1.1, colors='purple', linewidth=0.7, linestyles='--')
plt.xlabel('Years')
plt.ylabel('M(i) old')
plt.savefig('./PLOTS/' + workingtitle + '/annual_average_mi_single_old_S' + '_' + workingtitle + add_out + '.png')  # , dpi=150)
plt.savefig('./PLOTS/' + workingtitle + '/annual_average_mi_single_old_S' + '_' + workingtitle + add_out + '.eps')  # , dpi=150)
plt.ion()
plt.show(block=False)
plt.pause(0.01)
plt.close()



#calculate correlation coefficients
rho_m1_new_H = np.corrcoef(x=annual_trend_Mi_new_H[:,0], y=annual_trend_Mi_new_H[:,1])
rho_m1_old_H = np.corrcoef(x=annual_trend_Mi_old_H[:,0], y=annual_trend_Mi_old_H[:,1])
#plot annual trends, no errorbars A
plt.plot(annual_trend_Mi_old_H[:,0], annual_trend_Mi_old_H[:,1], '-', color="red", label=("M(i) old\nρ=" + str(round(rho_m1_old_H[0,1],3))))
plt.plot(annual_trend_Mi_new_H[:,0], annual_trend_Mi_new_H[:,1], '-', color="blue", label=("M(i) new\nρ=" + str(round(rho_m1_new_H[0,1],3))))
plt.legend(bbox_to_anchor=(0.73, 0.95), loc='upper left', borderaxespad=0.)
#plt.vlines(train_size - 1, 0, max(dataset) * 1.1, colors='purple', linewidth=0.7, linestyles='--')
plt.xlabel('Years')
plt.ylabel('Average M(i)')
plt.savefig('./PLOTS/' + workingtitle + '/annual_average_mi_H' + '_' + workingtitle + add_out + '.png')  # , dpi=150)
plt.savefig('./PLOTS/' + workingtitle + '/annual_average_mi_H' + '_' + workingtitle + add_out + '.eps')  # , dpi=150)
plt.ion()
plt.show(block=False)
plt.pause(0.01)
plt.close()
#plot annual trends, with errorbars
plt.errorbar(annual_trend_Mi_old_H[:,0], annual_trend_Mi_old_H[:,1], yerr=annual_trend_Mi_old_H[:,2], color="red", elinewidth=3.0, label=("M(i) old\nρ=" + str(round(rho_m1_old_H[0,1],3))))
plt.errorbar(annual_trend_Mi_new_H[:,0], annual_trend_Mi_new_H[:,1], yerr=annual_trend_Mi_new_H[:,2], color="blue", elinewidth=1.0, label=("M(i) new\nρ=" + str(round(rho_m1_new_H[0,1],3))))
plt.legend(bbox_to_anchor=(0.73, 0.95), loc='upper left', borderaxespad=0.)
#plt.vlines(train_size - 1, 0, max(dataset) * 1.1, colors='purple', linewidth=0.7, linestyles='--')
plt.xlabel('Years')
plt.ylabel('Average M(i)')
plt.savefig('./PLOTS/' + workingtitle + '/annual_average_mi_err_H' + '_' + workingtitle + add_out + '.png')  # , dpi=150)
plt.savefig('./PLOTS/' + workingtitle + '/annual_average_mi_err_H' + '_' + workingtitle + add_out + '.eps')  # , dpi=150)
plt.ion()
plt.show(block=False)
plt.pause(0.01)
plt.close()
#plot annual trends, no errorbars, single paths, new Mi
color7 = iter(cm.rainbow(np.linspace(0.2, 0.6, len(annual_trend_single_Mi_new_H))))
for i in range(len(annual_trend_single_Mi_new_H)):
    c7 = next(color7)
    plt.plot(annual_trend_single_Mi_new_H_t[i], annual_trend_single_Mi_new_H[i], color=c7)
#plt.plot(annual_trend_Mi_old[:,0], annual_trend_Mi_old[:,1], '-', color="red", label=("M(i) old\nρ=" + str(round(rho_m1_old[0,1],3))))
plt.plot(annual_trend_Mi_new_H[:,0], annual_trend_Mi_new_H[:,1], '-', color="red", label=("Average M(i) new\nρ=" + str(round(rho_m1_new_H[0,1],3))))
plt.legend(bbox_to_anchor=(0.73, 0.95), loc='upper left', borderaxespad=0.)
#plt.vlines(train_size - 1, 0, max(dataset) * 1.1, colors='purple', linewidth=0.7, linestyles='--')
plt.xlabel('Years')
plt.ylabel('M(i) new')
plt.savefig('./PLOTS/' + workingtitle + '/annual_average_mi_single_new_H' + '_' + workingtitle + add_out + '.png')  # , dpi=150)
plt.savefig('./PLOTS/' + workingtitle + '/annual_average_mi_single_new_H' + '_' + workingtitle + add_out + '.eps')  # , dpi=150)
plt.ion()
plt.show(block=False)
plt.pause(0.01)
plt.close()
#plot annual trends, no errorbars, single paths, old Mi
color8 = iter(cm.rainbow(np.linspace(0.2, 0.6, len(annual_trend_single_Mi_old_H))))
for i in range(len(annual_trend_single_Mi_old_H)):
    c8 = next(color8)
    plt.plot(annual_trend_single_Mi_old_H_t[i], annual_trend_single_Mi_old_H[i], color=c8)
#plt.plot(annual_trend_Mi_old[:,0], annual_trend_Mi_old[:,1], '-', color="red", label=("M(i) old\nρ=" + str(round(rho_m1_old[0,1],3))))
plt.plot(annual_trend_Mi_old_H[:,0], annual_trend_Mi_old_H[:,1], '-', color="red", label=("Average M(i) old\nρ=" + str(round(rho_m1_old_H[0,1],3))))
plt.legend(bbox_to_anchor=(0.73, 0.95), loc='upper left', borderaxespad=0.)
#plt.vlines(train_size - 1, 0, max(dataset) * 1.1, colors='purple', linewidth=0.7, linestyles='--')
plt.xlabel('Years')
plt.ylabel('M(i) old')
plt.savefig('./PLOTS/' + workingtitle + '/annual_average_mi_single_old_H' + '_' + workingtitle + add_out + '.png')  # , dpi=150)
plt.savefig('./PLOTS/' + workingtitle + '/annual_average_mi_single_old_H' + '_' + workingtitle + add_out + '.eps')  # , dpi=150)
plt.ion()
plt.show(block=False)
plt.pause(0.01)
plt.close()







########################################################################################################################
# END # PLOT ANNUAL TRENDS #############################################################################################
########################################################################################################################
#
#
#
#
#
#
#
#
#
#
########################################################################################################################
# START # OUTPUT STATISTICS ############################################################################################
########################################################################################################################
print('Calculate statistics, writing output')
#calculating and writing all statsitics into an output file
out_stat = "Output file for analyzing data set: " + str(data_path) + "\n" + "\n"
out_stat = out_stat + "workingtitle: " + str(workingtitle) + "\n"
if take_average_for_comparison: out_stat = out_stat + "Analysis done using the overall averages as reference points."  + "\n"
else: out_stat = out_stat + "Analysis done using the neutral benchmarks as reference points."  + "\n"
if use_std_dev_for_normalizing: out_stat = out_stat + "Analysis done using std. dev for normalizing."  + "\n"
else: out_stat = out_stat + "Analysis done using the range between the reference point and the minimum for normalizing."  + "\n"
#
out_stat = out_stat + "\n" + "Statistics, overall:\n\n"
#
out_stat = out_stat + "Minimum overall, M(i)_old: " + "\n" + str(np.min(Mi_old)) + "\n"
out_stat = out_stat + "Maximum overall, M(i)_old: " + "\n" + str(np.max(Mi_old)) + "\n"
out_stat = out_stat + "Mean overall, M(i)_old: " + "\n" + str(np.mean(Mi_old)) + "\n"
out_stat = out_stat + "Std. dev. overall, M(i)_old: " + "\n" + str(np.std(Mi_old)) + "\n\n"
out_stat = out_stat + "Minimum overall, M(i)_new: " + "\n" + str(np.min(Mi_new)) + "\n"
out_stat = out_stat + "Maximum overall, M(i)_new: " + "\n" + str(np.max(Mi_new)) + "\n"
out_stat = out_stat + "Mean overall, M(i)_new: " + "\n" + str(np.mean(Mi_new)) + "\n"
out_stat = out_stat + "Std. dev. overall, M(i)_new: " + "\n" + str(np.std(Mi_new)) + "\n\n"
out_stat = out_stat + "Minimum overall, MowingMachine: " + "\n" + str(df_overall_min(data_clean, "MowingMachine")) + "\n"
out_stat = out_stat + "Maximum overall, MowingMachine: " + "\n" + str(df_overall_max(data_clean, "MowingMachine")) + "\n"
out_stat = out_stat + "Mean overall, MowingMachine: " + "\n" + str(df_overall_average(data_clean, "MowingMachine")) + "\n"
out_stat = out_stat + "Std. dev. overall, MowingMachine: " + "\n" + str(df_overall_std_dev(data_clean, "MowingMachine")) + "\n\n"
out_stat = out_stat + "Minimum overall, MowingConditioner: " + "\n" + str(df_overall_min(data_clean, "MowingConditioner")) + "\n"
out_stat = out_stat + "Maximum overall, MowingConditioner: " + "\n" + str(df_overall_max(data_clean, "MowingConditioner")) + "\n"
out_stat = out_stat + "Mean overall, MowingConditioner: " + "\n" + str(df_overall_average(data_clean, "MowingConditioner")) + "\n"
out_stat = out_stat + "Std. dev. overall, MowingConditioner: " + "\n" + str(df_overall_std_dev(data_clean, "MowingConditioner")) + "\n\n"
out_stat = out_stat + "Minimum overall, CutHeight_cm: " + "\n" + str(df_overall_min(data_clean, "CutHeight_cm")) + "\n"
out_stat = out_stat + "Maximum overall, CutHeight_cm: " + "\n" + str(df_overall_max(data_clean, "CutHeight_cm")) + "\n"
out_stat = out_stat + "Mean overall, CutHeight_cm: " + "\n" + str(df_overall_average(data_clean, "CutHeight_cm")) + "\n"
out_stat = out_stat + "Std. dev. overall, CutHeight_cm: " + "\n" + str(df_overall_std_dev(data_clean, "CutHeight_cm")) + "\n\n"
out_stat = out_stat + "Minimum overall, CutWidth_m: " + "\n" + str(df_overall_min(data_clean, "CutWidth_m")) + "\n"
out_stat = out_stat + "Maximum overall, CutWidth_m: " + "\n" + str(df_overall_max(data_clean, "CutWidth_m")) + "\n"
out_stat = out_stat + "Mean overall, CutWidth_m: " + "\n" + str(df_overall_average(data_clean, "CutWidth_m")) + "\n"
out_stat = out_stat + "Std. dev. overall, CutWidth_m: " + "\n" + str(df_overall_std_dev(data_clean, "CutWidth_m")) + "\n\n"
#
out_stat = out_stat + "Statistics, separate for each cut:\n\n"
#
out_stat = out_stat + "Cut 1:\n"
out_stat = out_stat + "Minimum, MowingMachine1: " + "\n" + str(df_min(data_clean, "MowingMachine1")) + "\n"
out_stat = out_stat + "Maximum, MowingMachine1: " + "\n" + str(df_max(data_clean, "MowingMachine1")) + "\n"
out_stat = out_stat + "Mean, MowingMachine1: " + "\n" + str(df_average(data_clean, "MowingMachine1")) + "\n"
out_stat = out_stat + "Std. dev., MowingMachine1: " + "\n" + str(df_std_dev(data_clean, "MowingMachine1")) + "\n\n"
out_stat = out_stat + "Minimum, MowingConditioner1: " + "\n" + str(df_min(data_clean, "MowingConditioner1")) + "\n"
out_stat = out_stat + "Maximum, MowingConditioner1: " + "\n" + str(df_max(data_clean, "MowingConditioner1")) + "\n"
out_stat = out_stat + "Mean, MowingConditioner1: " + "\n" + str(df_average(data_clean, "MowingConditioner1")) + "\n"
out_stat = out_stat + "Std. dev., MowingConditioner1: " + "\n" + str(df_std_dev(data_clean, "MowingConditioner1")) + "\n\n"
out_stat = out_stat + "Minimum, CutHeight_cm1: " + "\n" + str(df_min(data_clean, "CutHeight_cm1")) + "\n"
out_stat = out_stat + "Maximum, CutHeight_cm1: " + "\n" + str(df_max(data_clean, "CutHeight_cm1")) + "\n"
out_stat = out_stat + "Mean, CutHeight_cm1: " + "\n" + str(df_average(data_clean, "CutHeight_cm1")) + "\n"
out_stat = out_stat + "Std. dev., CutHeight_cm1: " + "\n" + str(df_std_dev(data_clean, "CutHeight_cm1")) + "\n\n"
out_stat = out_stat + "Minimum, CutWidth_m1: " + "\n" + str(df_min(data_clean, "CutWidth_m1")) + "\n"
out_stat = out_stat + "Maximum, CutWidth_m1: " + "\n" + str(df_max(data_clean, "CutWidth_m1")) + "\n"
out_stat = out_stat + "Mean, CutWidth_m1: " + "\n" + str(df_average(data_clean, "CutWidth_m1")) + "\n"
out_stat = out_stat + "Std. dev., CutWidth_m1: " + "\n" + str(df_std_dev(data_clean, "CutWidth_m1")) + "\n\n"
#
out_stat = out_stat + "Cut 2:\n"
out_stat = out_stat + "Minimum, MowingMachine2: " + "\n" + str(df_min(data_clean, "MowingMachine2")) + "\n"
out_stat = out_stat + "Maximum, MowingMachine2: " + "\n" + str(df_max(data_clean, "MowingMachine2")) + "\n"
out_stat = out_stat + "Mean, MowingMachine2: " + "\n" + str(df_average(data_clean, "MowingMachine2")) + "\n"
out_stat = out_stat + "Std. dev., MowingMachine2: " + "\n" + str(df_std_dev(data_clean, "MowingMachine2")) + "\n\n"
out_stat = out_stat + "Minimum, MowingConditioner2: " + "\n" + str(df_min(data_clean, "MowingConditioner2")) + "\n"
out_stat = out_stat + "Maximum, MowingConditioner2: " + "\n" + str(df_max(data_clean, "MowingConditioner2")) + "\n"
out_stat = out_stat + "Mean, MowingConditioner2: " + "\n" + str(df_average(data_clean, "MowingConditioner2")) + "\n"
out_stat = out_stat + "Std. dev., MowingConditioner2: " + "\n" + str(df_std_dev(data_clean, "MowingConditioner2")) + "\n\n"
out_stat = out_stat + "Minimum, CutHeight_cm2: " + "\n" + str(df_min(data_clean, "CutHeight_cm2")) + "\n"
out_stat = out_stat + "Maximum, CutHeight_cm2: " + "\n" + str(df_max(data_clean, "CutHeight_cm2")) + "\n"
out_stat = out_stat + "Mean, CutHeight_cm2: " + "\n" + str(df_average(data_clean, "CutHeight_cm2")) + "\n"
out_stat = out_stat + "Std. dev., CutHeight_cm2: " + "\n" + str(df_std_dev(data_clean, "CutHeight_cm2")) + "\n\n"
out_stat = out_stat + "Minimum, CutWidth_m2: " + "\n" + str(df_min(data_clean, "CutWidth_m2")) + "\n"
out_stat = out_stat + "Maximum, CutWidth_m2: " + "\n" + str(df_max(data_clean, "CutWidth_m2")) + "\n"
out_stat = out_stat + "Mean, CutWidth_m2: " + "\n" + str(df_average(data_clean, "CutWidth_m2")) + "\n"
out_stat = out_stat + "Std. dev., CutWidth_m2: " + "\n" + str(df_std_dev(data_clean, "CutWidth_m2")) + "\n\n"
#
out_stat = out_stat + "Cut 3:\n"
out_stat = out_stat + "Minimum, MowingMachine3: " + "\n" + str(df_min(data_clean, "MowingMachine3")) + "\n"
out_stat = out_stat + "Maximum, MowingMachine3: " + "\n" + str(df_max(data_clean, "MowingMachine3")) + "\n"
out_stat = out_stat + "Mean, MowingMachine3: " + "\n" + str(df_average(data_clean, "MowingMachine3")) + "\n"
out_stat = out_stat + "Std. dev., MowingMachine3: " + "\n" + str(df_std_dev(data_clean, "MowingMachine3")) + "\n\n"
out_stat = out_stat + "Minimum, MowingConditioner3: " + "\n" + str(df_min(data_clean, "MowingConditioner3")) + "\n"
out_stat = out_stat + "Maximum, MowingConditioner3: " + "\n" + str(df_max(data_clean, "MowingConditioner3")) + "\n"
out_stat = out_stat + "Mean, MowingConditioner3: " + "\n" + str(df_average(data_clean, "MowingConditioner3")) + "\n"
out_stat = out_stat + "Std. dev., MowingConditioner3: " + "\n" + str(df_std_dev(data_clean, "MowingConditioner3")) + "\n\n"
out_stat = out_stat + "Minimum, CutHeight_cm3: " + "\n" + str(df_min(data_clean, "CutHeight_cm3")) + "\n"
out_stat = out_stat + "Maximum, CutHeight_cm3: " + "\n" + str(df_max(data_clean, "CutHeight_cm3")) + "\n"
out_stat = out_stat + "Mean, CutHeight_cm3: " + "\n" + str(df_average(data_clean, "CutHeight_cm3")) + "\n"
out_stat = out_stat + "Std. dev., CutHeight_cm3: " + "\n" + str(df_std_dev(data_clean, "CutHeight_cm3")) + "\n\n"
out_stat = out_stat + "Minimum, CutWidth_m3: " + "\n" + str(df_min(data_clean, "CutWidth_m3")) + "\n"
out_stat = out_stat + "Maximum, CutWidth_m3: " + "\n" + str(df_max(data_clean, "CutWidth_m3")) + "\n"
out_stat = out_stat + "Mean, CutWidth_m3: " + "\n" + str(df_average(data_clean, "CutWidth_m3")) + "\n"
out_stat = out_stat + "Std. dev., CutWidth_m3: " + "\n" + str(df_std_dev(data_clean, "CutWidth_m3")) + "\n\n"
#
out_stat = out_stat + "Cut 4:\n"
out_stat = out_stat + "Minimum, MowingMachine4: " + "\n" + str(df_min(data_clean, "MowingMachine4")) + "\n"
out_stat = out_stat + "Maximum, MowingMachine4: " + "\n" + str(df_max(data_clean, "MowingMachine4")) + "\n"
out_stat = out_stat + "Mean, MowingMachine4: " + "\n" + str(df_average(data_clean, "MowingMachine4")) + "\n"
out_stat = out_stat + "Std. dev., MowingMachine4: " + "\n" + str(df_std_dev(data_clean, "MowingMachine4")) + "\n\n"
out_stat = out_stat + "Minimum, MowingConditioner4: " + "\n" + str(df_min(data_clean, "MowingConditioner4")) + "\n"
out_stat = out_stat + "Maximum, MowingConditioner4: " + "\n" + str(df_max(data_clean, "MowingConditioner4")) + "\n"
out_stat = out_stat + "Mean, MowingConditioner4: " + "\n" + str(df_average(data_clean, "MowingConditioner4")) + "\n"
out_stat = out_stat + "Std. dev., MowingConditioner4: " + "\n" + str(df_std_dev(data_clean, "MowingConditioner4")) + "\n\n"
out_stat = out_stat + "Minimum, CutHeight_cm4: " + "\n" + str(df_min(data_clean, "CutHeight_cm4")) + "\n"
out_stat = out_stat + "Maximum, CutHeight_cm4: " + "\n" + str(df_max(data_clean, "CutHeight_cm4")) + "\n"
out_stat = out_stat + "Mean, CutHeight_cm4: " + "\n" + str(df_average(data_clean, "CutHeight_cm4")) + "\n"
out_stat = out_stat + "Std. dev., CutHeight_cm4: " + "\n" + str(df_std_dev(data_clean, "CutHeight_cm4")) + "\n\n"
out_stat = out_stat + "Minimum, CutWidth_m4: " + "\n" + str(df_min(data_clean, "CutWidth_m4")) + "\n"
out_stat = out_stat + "Maximum, CutWidth_m4: " + "\n" + str(df_max(data_clean, "CutWidth_m4")) + "\n"
out_stat = out_stat + "Mean, CutWidth_m4: " + "\n" + str(df_average(data_clean, "CutWidth_m4")) + "\n"
out_stat = out_stat + "Std. dev., CutWidth_m4: " + "\n" + str(df_std_dev(data_clean, "CutWidth_m4")) + "\n\n"
#
out_stat = out_stat + "Cut 5:\n"
out_stat = out_stat + "Minimum, MowingMachine5: " + "\n" + str(df_min(data_clean, "MowingMachine5")) + "\n"
out_stat = out_stat + "Maximum, MowingMachine5: " + "\n" + str(df_max(data_clean, "MowingMachine5")) + "\n"
out_stat = out_stat + "Mean, MowingMachine5: " + "\n" + str(df_average(data_clean, "MowingMachine5")) + "\n"
out_stat = out_stat + "Std. dev., MowingMachine5: " + "\n" + str(df_std_dev(data_clean, "MowingMachine5")) + "\n\n"
out_stat = out_stat + "Minimum, MowingConditioner5: " + "\n" + str(df_min(data_clean, "MowingConditioner5")) + "\n"
out_stat = out_stat + "Maximum, MowingConditioner5: " + "\n" + str(df_max(data_clean, "MowingConditioner5")) + "\n"
out_stat = out_stat + "Mean, MowingConditioner5: " + "\n" + str(df_average(data_clean, "MowingConditioner5")) + "\n"
out_stat = out_stat + "Std. dev., MowingConditioner5: " + "\n" + str(df_std_dev(data_clean, "MowingConditioner5")) + "\n\n"
out_stat = out_stat + "Minimum, CutHeight_cm5: " + "\n" + str(df_min(data_clean, "CutHeight_cm5")) + "\n"
out_stat = out_stat + "Maximum, CutHeight_cm5: " + "\n" + str(df_max(data_clean, "CutHeight_cm5")) + "\n"
out_stat = out_stat + "Mean, CutHeight_cm5: " + "\n" + str(df_average(data_clean, "CutHeight_cm5")) + "\n"
out_stat = out_stat + "Std. dev., CutHeight_cm5: " + "\n" + str(df_std_dev(data_clean, "CutHeight_cm5")) + "\n\n"
out_stat = out_stat + "Minimum, CutWidth_m5: " + "\n" + str(df_min(data_clean, "CutWidth_m5")) + "\n"
out_stat = out_stat + "Maximum, CutWidth_m5: " + "\n" + str(df_max(data_clean, "CutWidth_m5")) + "\n"
out_stat = out_stat + "Mean, CutWidth_m5: " + "\n" + str(df_average(data_clean, "CutWidth_m5")) + "\n"
out_stat = out_stat + "Std. dev., CutWidth_m5: " + "\n" + str(df_std_dev(data_clean, "CutWidth_m5")) + "\n\n"
#
# save results:
InfoPath = './ANALYSIS/' + workingtitle + '/stats_out_' + workingtitle + add_out + '.txt'
InfoFile = open(InfoPath, "w+")
InfoFile.write(out_stat)
InfoFile.close()
########################################################################################################################
## END ## OUTPUT STATISTICS ############################################################################################
########################################################################################################################
#
print('Done')
