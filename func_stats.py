import numpy as np
import pandas as pd
from copy import deepcopy as dc

def df_average(data_pd, column):
    """
    This function calculates the average for each variable by leaving out the -1s
    """
    data_np = dc(data_pd[column].to_numpy(dtype=float))
    av_list = list()
    for i in range(len(data_np)):
        if data_np[i] >0.1:
            av_list.append(data_np[i])
    av_list = dc(np.array(av_list, dtype=float))
    return np.mean(av_list)

def df_std_dev(data_pd, column):
    """
    This function calculates the standard error for each variable by leaving out the -1s
    """
    data_np = dc(data_pd[column].to_numpy(dtype=float))
    av_list = list()
    for i in range(len(data_np)):
        if data_np[i] >0.1:
            av_list.append(data_np[i])
    av_list = dc(np.array(av_list, dtype=float))
    return np.std(av_list)

def df_overall_average(data_pd, column_pre_fix):
    """
    This function calculates the average for each variable, and all instances, i.e. 1,2,3,..., by leaving out the -1s
    """
    av_list = list()
    for i in range(5):
        pre_fix_add = dc(str(i+1))
        data_np = dc(data_pd[str(column_pre_fix) + pre_fix_add].to_numpy(dtype=float))
        for i in range(len(data_np)):
            if data_np[i] > 0.1:
                av_list.append(data_np[i])
    av_list = dc(np.array(av_list, dtype=float))
    return np.mean(av_list)

def df_overall_std_dev(data_pd, column_pre_fix):
    """
    This function calculates the std dev for each variable, and all instances, i.e. 1,2,3,..., by leaving out the -1s
    """
    av_list = list()
    for i in range(5):
        pre_fix_add = dc(str(i+1))
        data_np = dc(data_pd[str(column_pre_fix) + pre_fix_add].to_numpy(dtype=float))
        for i in range(len(data_np)):
            if data_np[i] > 0.1:
                av_list.append(data_np[i])
    av_list = dc(np.array(av_list, dtype=float))
    return np.std(av_list)

def df_overall_min_max(data_pd, column_pre_fix):
    """
    This function gives min and max for each variable
    """
    av_list = list()
    for i in range(5):
        pre_fix_add = dc(str(i+1))
        data_np = dc(data_pd[str(column_pre_fix) + pre_fix_add].to_numpy(dtype=float))
        for i in range(len(data_np)):
            if data_np[i] > 0.1:
                av_list.append(data_np[i])
    av_list = dc(np.array(av_list, dtype=float))
    return np.min(av_list), np.max(av_list)

def df_overall_min(data_pd, column_pre_fix):
    """
    This function gives min for each variable
    """
    av_list = list()
    for i in range(5):
        pre_fix_add = dc(str(i+1))
        data_np = dc(data_pd[str(column_pre_fix) + pre_fix_add].to_numpy(dtype=float))
        for i in range(len(data_np)):
            if data_np[i] > 0.1:
                av_list.append(data_np[i])
    av_list = dc(np.array(av_list, dtype=float))
    return np.min(av_list)

def df_overall_max(data_pd, column_pre_fix):
    """
    This function gives max for each variable
    """
    av_list = list()
    for i in range(5):
        pre_fix_add = dc(str(i+1))
        data_np = dc(data_pd[str(column_pre_fix) + pre_fix_add].to_numpy(dtype=float))
        for i in range(len(data_np)):
            if data_np[i] > 0.1:
                av_list.append(data_np[i])
    av_list = dc(np.array(av_list, dtype=float))
    return np.max(av_list)

def df_min(data_pd, column):
    """
    This function calculates the min for each variable by leaving out the -1s
    """
    data_np = dc(data_pd[column].to_numpy(dtype=float))
    av_list = list()
    for i in range(len(data_np)):
        if data_np[i] >0.1:
            av_list.append(data_np[i])
    av_list = dc(np.array(av_list, dtype=float))
    return np.min(av_list)

def df_max(data_pd, column):
    """
    This function calculates the max for each variable by leaving out the -1s
    """
    data_np = dc(data_pd[column].to_numpy(dtype=float))
    av_list = list()
    for i in range(len(data_np)):
        if data_np[i] >0.1:
            av_list.append(data_np[i])
    av_list = dc(np.array(av_list, dtype=float))
    return np.max(av_list)


