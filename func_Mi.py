import numpy as np
import pandas as pd
from func_stats import df_average, df_std_dev, df_overall_average, df_overall_std_dev, df_overall_min_max
from copy import deepcopy as dc
import sys

def old_Mi(data_pd):
    """
    Input: Pandas dataframe
    Output: numpy array containing the old Mi for each site,
    """
    return dc(data_pd['Cuts'].to_numpy())

def replace_negatives_with_averages(data_pd):
    """
    Replace -200.0 values in MowingMachine, CutWidth_m, and CutHeight_cm with their respective averages
    """
    data_pd = dc(data_pd)

    # Calculate the averages for each column
    average_mowing_machine = data_pd['MowingMachine'].replace(-200.0, pd.NA).mean()
    average_cut_width = data_pd['CutWidth_m'].replace(-200.0, pd.NA).mean()
    average_cut_height = data_pd['CutHeight_cm'].replace(-200.0, pd.NA).mean()
    average_mowing_machine = data_pd['MowingConditioner'].replace(-200.0, pd.NA).mean()


    # Replace -200.0 with the respective averages
    data_pd['MowingMachine'] = data_pd['MowingMachine'].replace(-200.0, average_mowing_machine)
    data_pd['CutWidth_m'] = data_pd['CutWidth_m'].replace(-200.0, average_cut_width)
    data_pd['CutHeight_cm'] = data_pd['CutHeight_cm'].replace(-200.0, average_cut_height)
    average_mowing_machine = data_pd['MowingConditioner'].replace(-200.0, pd.NA).mean()
    return data_pd



def new_Mi_range(data_pd, set_average=False, use_std_dev = False, params=None, keep_everything=False):
    """
    Calculate new M(i), see paper

    """
    if params == None:
        #first calc mean, std dev, min, max, and set the sign
        alpha_MowingMachine = 1
        alpha_CutWidth = -1
        alpha_CutHeight = -1
        alpha_MowingConditioner = 1
    else:
        alpha_MowingMachine = params[0]
        alpha_CutWidth = params[1]
        alpha_CutHeight = params[2]
        alpha_MowingConditioner = params[3]

    Min_MowingMachine1, Max_MowingMachine1 = df_overall_min_max(data_pd, "MowingMachine")
    #actually Min_MowingMachine1 needs to be zero, but zero is not present in the data


    Min_MowingConditioner1, Max_MowingConditioner1 = df_overall_min_max(data_pd, "MowingConditioner")
    Min_CutWidth_m1, Max_CutWidth_m1 = df_overall_min_max(data_pd, "CutWidth_m")
    Min_CutHeight_cm1, Max_CutHeight_cm1 = df_overall_min_max(data_pd, "CutHeight_cm")

    #
    Average_MowingMachine1 = df_overall_average(data_pd, "MowingMachine")
    Average_MowingConditioner1 = df_overall_average(data_pd, "MowingConditioner")
    Average_CutWidth_m1 = df_overall_average(data_pd, "CutWidth_m")
    Average_CutHeight_cm1 = df_overall_average(data_pd, "CutHeight_cm")
    #
    Min_MowingMachine1 = 0
    Min_CutWidth_m1 = 1.8
    Min_CutHeight_cm1 = 3.0
    Min_MowingConditioner1 = 0

    if set_average == False:
        Average_CutHeight_cm1 = 10
        Average_MowingMachine1 = 1 #df_average(data_pd, "MowingMachine1")
        Average_MowingConditioner1 = 0.5 #df_average(data_pd, "MowingConditioner1")
        #Average_CutWidth_m1 = 2.55
    if use_std_dev:
        Std_Dev_MowingMachine1 = df_overall_std_dev(data_pd, "MowingMachine")
        Std_Dev_MowingConditioner1 = df_overall_std_dev(data_pd, "MowingConditioner")
        Std_Dev_CutWidth_m1 = df_overall_std_dev(data_pd, "CutWidth_m")
        Std_Dev_CutHeight_cm1 = df_overall_std_dev(data_pd, "CutHeight_cm")
    else:
        Std_Dev_CutWidth_m1 = Average_CutWidth_m1 - Min_CutWidth_m1
        Std_Dev_CutHeight_cm1 = Average_CutHeight_cm1 - Min_CutHeight_cm1
        Std_Dev_MowingConditioner1 = Average_MowingConditioner1 - Min_MowingConditioner1
        Std_Dev_MowingMachine1 = Average_MowingMachine1 - Min_MowingMachine1
    #


    data_pd_in = dc(data_pd)
    Mi_list = list()
    #
    for index, row in data_pd_in.iterrows():
        #
        Mi = 0
        #
        if row['Cuts'] > 0:
            AddVari_1 = alpha_MowingMachine * ((row['MowingMachine1'] - Average_MowingMachine1)/(Std_Dev_MowingMachine1+0.0000001))
            AddVari_1 = AddVari_1 + alpha_MowingConditioner * ((row['MowingConditioner1'] - Average_MowingConditioner1)/(Std_Dev_MowingConditioner1+0.0000001))
            AddVari_1 = AddVari_1 + alpha_CutWidth * ((row['CutWidth_m1'] - Average_CutWidth_m1)/(Std_Dev_CutWidth_m1+0.0000001))
            AddVari_1 = AddVari_1 + alpha_CutHeight * ((row['CutHeight_cm1'] - Average_CutHeight_cm1)/(Std_Dev_CutHeight_cm1+0.0000001))
            Mi = 1 + (1/row['Cuts']) * (AddVari_1/4)
        if row['Cuts'] > 1:
            AddVari_2 = alpha_MowingMachine * ((row['MowingMachine2'] - Average_MowingMachine1)/(Std_Dev_MowingMachine1+0.0000001))
            AddVari_2 = AddVari_2 + alpha_MowingConditioner * ((row['MowingConditioner2'] - Average_MowingConditioner1)/(Std_Dev_MowingConditioner1+0.0000001))
            AddVari_2 = AddVari_2 + alpha_CutWidth * ((row['CutWidth_m2'] - Average_CutWidth_m1)/(Std_Dev_CutWidth_m1+0.0000001))
            AddVari_2 = AddVari_2 + alpha_CutHeight * ((row['CutHeight_cm2'] - Average_CutHeight_cm1)/(Std_Dev_CutHeight_cm1+0.0000001))
            Mi = Mi + 1 + (1/row['Cuts']) * (AddVari_2/4)
        if row['Cuts'] > 2:
            AddVari_3 = alpha_MowingMachine * ((row['MowingMachine3'] - Average_MowingMachine1)/(Std_Dev_MowingMachine1+0.0000001))
            AddVari_3 = AddVari_3 + alpha_MowingConditioner * ((row['MowingConditioner3'] - Average_MowingConditioner1)/(Std_Dev_MowingConditioner1+0.0000001))
            AddVari_3 = AddVari_3 + alpha_CutWidth * ((row['CutWidth_m3'] - Average_CutWidth_m1)/(Std_Dev_CutWidth_m1+0.0000001))
            AddVari_3 = AddVari_3 + alpha_CutHeight * ((row['CutHeight_cm3'] - Average_CutHeight_cm1)/(Std_Dev_CutHeight_cm1+0.0000001))
            Mi = Mi + 1 + (1/row['Cuts']) * (AddVari_3/4)
        if row['Cuts'] > 3:
            AddVari_4 = alpha_MowingMachine * ((row['MowingMachine4'] - Average_MowingMachine1)/(Std_Dev_MowingMachine1+0.0000001))
            AddVari_4 = AddVari_4 + alpha_MowingConditioner * ((row['MowingConditioner4'] - Average_MowingConditioner1)/(Std_Dev_MowingConditioner1+0.0000001))
            AddVari_4 = AddVari_4 + alpha_CutWidth * ((row['CutWidth_m4'] - Average_CutWidth_m1)/(Std_Dev_CutWidth_m1+0.0000001))
            AddVari_4 = AddVari_4 + alpha_CutHeight * ((row['CutHeight_cm4'] - Average_CutHeight_cm1)/(Std_Dev_CutHeight_cm1+0.0000001))
            Mi = Mi + 1 + (1/row['Cuts']) * (AddVari_4/4)
        if row['Cuts'] > 4:
            AddVari_5 = alpha_MowingMachine * ((row['MowingMachine5'] - Average_MowingMachine1)/(Std_Dev_MowingMachine1+0.0000001))
            AddVari_5 = AddVari_5 + alpha_MowingConditioner * ((row['MowingConditioner5'] - Average_MowingConditioner1)/(Std_Dev_MowingConditioner1+0.0000001))
            AddVari_5 = AddVari_5 + alpha_CutWidth * ((row['CutWidth_m5'] - Average_CutWidth_m1)/(Std_Dev_CutWidth_m1+0.0000001))
            AddVari_5 = AddVari_5 + alpha_CutHeight * ((row['CutHeight_cm5'] - Average_CutHeight_cm1)/(Std_Dev_CutHeight_cm1+0.0000001))
            Mi = Mi + 1 + (1/row['Cuts']) * (AddVari_5/4)
        #
        Mi_list.append(Mi)
    #
    return dc(np.array(Mi_list, dtype=float))

def new_Mi_range_no_width(data_pd, set_average=False, use_std_dev = False, params = None, keep_everything=False):
    """
    Calculate new M(i), see equation X

    """
    if params == None:
        #first calc mean, std dev, min, max, and set the sign
        alpha_MowingMachine = 1
        alpha_CutHeight = -1
        alpha_MowingConditioner = 1
    else:
        alpha_MowingMachine = params[0]
        alpha_CutHeight = params[2]
        alpha_MowingConditioner = params[3]

    Min_MowingMachine1, Max_MowingMachine1 = df_overall_min_max(data_pd, "MowingMachine")
    #actually Min_MowingMachine1 needs to be zero, but zero is not present in the data
    Min_MowingMachine1 = 0

    Min_MowingConditioner1 = 0
    Min_MowingConditioner1 = 1
    Min_CutHeight_cm1, Max_CutHeight_cm1 = df_overall_min_max(data_pd, "CutHeight_cm")

    #
    Average_MowingMachine1 = df_overall_average(data_pd, "MowingMachine")
    Average_MowingConditioner1 = df_overall_average(data_pd, "MowingConditioner")
    Average_CutHeight_cm1 = df_overall_average(data_pd, "CutHeight_cm")
    #
    Min_MowingMachine1 = 0
    Min_CutWidth_m1 = 1.8
    Min_CutHeight_cm1 = 3.0
    Min_MowingConditioner1 = 0
    #
    if set_average == False:
        Average_CutHeight_cm1 = 10
        Average_MowingMachine1 = 1 #df_average(data_pd, "MowingMachine1")
        Average_MowingConditioner1 = 0.5 #df_average(data_pd, "MowingConditioner1")
    if use_std_dev:
        Std_Dev_MowingMachine1 = df_overall_std_dev(data_pd, "MowingMachine")
        Std_Dev_MowingConditioner1 = df_overall_std_dev(data_pd, "MowingConditioner")
        Std_Dev_CutHeight_cm1 = df_overall_std_dev(data_pd, "CutHeight_cm")
    else:
        Std_Dev_CutHeight_cm1 = Average_CutHeight_cm1 - Min_CutHeight_cm1
        Std_Dev_MowingConditioner1 = Average_MowingConditioner1 - Min_MowingConditioner1
        Std_Dev_MowingMachine1 = Average_MowingMachine1 - Min_MowingMachine1

    data_pd_in = dc(data_pd)
    Mi_list = list()
    #
    for index, row in data_pd_in.iterrows():
        #
        Mi = 0
        #
        if row['Cuts'] > 0:
            AddVari_1 = alpha_MowingMachine * ((row['MowingMachine1'] - Average_MowingMachine1)/(Std_Dev_MowingMachine1+0.0000001))
            AddVari_1 = AddVari_1 + alpha_MowingConditioner * ((row['MowingConditioner1'] - Average_MowingConditioner1)/(Std_Dev_MowingConditioner1+0.0000001))
            AddVari_1 = AddVari_1 + alpha_CutHeight * ((row['CutHeight_cm1'] - Average_CutHeight_cm1)/(Std_Dev_CutHeight_cm1+0.0000001))
            Mi = 1 + (1/row['Cuts']) * (AddVari_1/3)
        if row['Cuts'] > 1:
            AddVari_2 = alpha_MowingMachine * ((row['MowingMachine2'] - Average_MowingMachine1)/(Std_Dev_MowingMachine1+0.0000001))
            AddVari_2 = AddVari_2 + alpha_MowingConditioner * ((row['MowingConditioner2'] - Average_MowingConditioner1)/(Std_Dev_MowingConditioner1+0.0000001))
            AddVari_2 = AddVari_2 + alpha_CutHeight * ((row['CutHeight_cm2'] - Average_CutHeight_cm1)/(Std_Dev_CutHeight_cm1+0.0000001))
            Mi = Mi + 1 + (1/row['Cuts']) * (AddVari_2/3)
        if row['Cuts'] > 2:
            AddVari_3 = alpha_MowingMachine * ((row['MowingMachine3'] - Average_MowingMachine1)/(Std_Dev_MowingMachine1+0.0000001))
            AddVari_3 = AddVari_3 + alpha_MowingConditioner * ((row['MowingConditioner3'] - Average_MowingConditioner1)/(Std_Dev_MowingConditioner1+0.0000001))
            AddVari_3 = AddVari_3 + alpha_CutHeight * ((row['CutHeight_cm3'] - Average_CutHeight_cm1)/(Std_Dev_CutHeight_cm1+0.0000001))
            Mi = Mi + 1 + (1/row['Cuts']) * (AddVari_3/3)
        if row['Cuts'] > 3:
            AddVari_4 = alpha_MowingMachine * ((row['MowingMachine4'] - Average_MowingMachine1)/(Std_Dev_MowingMachine1+0.0000001))
            AddVari_4 = AddVari_4 + alpha_MowingConditioner * ((row['MowingConditioner4'] - Average_MowingConditioner1)/(Std_Dev_MowingConditioner1+0.0000001))
            AddVari_4 = AddVari_4 + alpha_CutHeight * ((row['CutHeight_cm4'] - Average_CutHeight_cm1)/(Std_Dev_CutHeight_cm1+0.0000001))
            Mi = Mi + 1 + (1/row['Cuts']) * (AddVari_4/3)
        if row['Cuts'] > 4:
            AddVari_5 = alpha_MowingMachine * ((row['MowingMachine5'] - Average_MowingMachine1)/(Std_Dev_MowingMachine1+0.0000001))
            AddVari_5 = AddVari_5 + alpha_MowingConditioner * ((row['MowingConditioner5'] - Average_MowingConditioner1)/(Std_Dev_MowingConditioner1+0.0000001))
            AddVari_5 = AddVari_5 + alpha_CutHeight * ((row['CutHeight_cm5'] - Average_CutHeight_cm1)/(Std_Dev_CutHeight_cm1+0.0000001))
            Mi = Mi + 1 + (1/row['Cuts']) * (AddVari_5/3)
        #
        Mi_list.append(Mi)
    #
    return dc(np.array(Mi_list, dtype=float))


def new_Mi_range_no_width_no_conditioner(data_pd, set_average=False, use_std_dev=False, params=None, keep_everything=False):
    """
    Calculate new M(i), see equation X
    """

    print("Function no conditioner")
    if params is None:
        # Default parameters
        alpha_MowingMachine = 1
        alpha_CutHeight = -1
        alpha_MowingConditioner = 1
    else:
        alpha_MowingMachine = params[0]
        alpha_CutHeight = params[2]
        alpha_MowingConditioner = params[3]

    # Fetch overall statistics
    Min_MowingMachine1, Max_MowingMachine1 = df_overall_min_max(data_pd, "MowingMachine")
    Min_MowingMachine1 = 0

    Min_CutHeight_cm1, Max_CutHeight_cm1 = df_overall_min_max(data_pd, "CutHeight_cm")

    Average_MowingMachine1 = df_overall_average(data_pd, "MowingMachine")
    Average_MowingConditioner1 = df_overall_average(data_pd, "MowingConditioner")
    Average_CutHeight_cm1 = df_overall_average(data_pd, "CutHeight_cm")

    Min_CutWidth_m1 = 1.8
    Min_CutHeight_cm1 = 3.0
    Min_MowingConditioner1 = 0

    if not set_average:
        Average_CutHeight_cm1 = 10
        Average_MowingMachine1 = 1
        Average_MowingConditioner1 = 0.5

    if use_std_dev:
        Std_Dev_MowingMachine1 = df_overall_std_dev(data_pd, "MowingMachine")
        Std_Dev_MowingConditioner1 = df_overall_std_dev(data_pd, "MowingConditioner")
        Std_Dev_CutHeight_cm1 = df_overall_std_dev(data_pd, "CutHeight_cm")
    else:
        Std_Dev_CutHeight_cm1 = Average_CutHeight_cm1 - Min_CutHeight_cm1
        Std_Dev_MowingConditioner1 = Average_MowingConditioner1 - Min_MowingConditioner1
        Std_Dev_MowingMachine1 = Average_MowingMachine1 - Min_MowingMachine1

    data_pd_in = dc(data_pd)
    Mi_list = []

    for index, row in data_pd_in.iterrows():
        Mi = 0
        for cut in range(1, int(row['Cuts']) + 1):
            AddVari = alpha_MowingMachine * ((row[f'MowingMachine{cut}'] - Average_MowingMachine1) / (Std_Dev_MowingMachine1 + 1e-7))
            AddVari += alpha_CutHeight * ((row[f'CutHeight_cm{cut}'] - Average_CutHeight_cm1) / (Std_Dev_CutHeight_cm1 + 1e-7))
            Mi += 1 + (1 / row['Cuts']) * (AddVari / 2)
        Mi_list.append(Mi)

    return dc(np.array(Mi_list, dtype=float))






def Mowingmachine(data_pd, set_average=False, use_std_dev = False, params=None):
    """
    output the seperate variables for the first cut
    """

    #
    data_pd_in = dc(data_pd)
    Mi_list = list()
    MowingMachine_list = list()
    MowingConditioner_list = list()
    CutWidth_list = list()
    CutHeight_list = list()
    year_list = list()
    EP_Plot_list = list()
    #
    for index, row in data_pd_in.iterrows():
        #
        Mi = 0
        #
        if row['Cuts'] > 0:
            year_list.append(row['Year'])
            EP_Plot_list.append(row['EP_PlotID'])
            MowingMachine_list.append(row['MowingMachine1'])
            MowingConditioner_list.append(row['MowingConditioner1'])
            CutWidth_list.append(row['CutWidth_m1'])
            CutHeight_list.append(row['CutHeight_cm1'])
        #
        Mi_list.append(Mi)
    #
    return year_list, EP_Plot_list, MowingMachine_list, MowingConditioner_list, CutWidth_list, CutHeight_list










def sort_Mi_regional(Mi_out_arr, first_letter_region, header_out):
    Mi_out_calc = dc(Mi_out_arr[1:,:])
    Mi_out = list()
    Mi_out.append(header_out)
    for i in range(len(Mi_out_calc[:,1])):
        test_str = dc(str(Mi_out_calc[i,1]))
        #print(test_str)
        if test_str[0] == first_letter_region:
            #print('found')
            Mi_out.append(Mi_out_calc[i,:])
    #output and rearranging dtype
    return dc(np.array(Mi_out, dtype=object))

