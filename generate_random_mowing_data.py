from faker import Faker
import pandas as pd
import random

# instantiate a faker
fake = Faker()

# define the number of rows to be generated
n_rows = 300  # replace with your preferred number of rows

def fake_data(n):
    # Define mowing machines and conditioner choices
    mowing_machines = ["Kreiselmäher", "Kreiselmäher, Mulcher", "Mulchgerät", "Mulcher", "Scheibenmähwerk", "Balkenmäher", "Fingermähwerk", "Doppelmesser", "Sense"]
    conditioner = ["ja", "nein"]

    words = ["AEG", "HEG", "SEG"]

    data = {
        "EP_PlotID": [f"{random.choice(words)}{random.randint(1, 99)}" for _ in range(n)],
        "Year": [fake.year() for _ in range(n)],
        "Cuts": [random.randint(1, 5) for _ in range(n)]
    }
    for i in range(1, 6):
        data[f"MowingMachine{i}"] = [random.choice(mowing_machines) for _ in range(n)]
        data[f"CutWidth_m{i}"] = [random.randint(1, 10) for _ in range(n)]
        data[f"CutHeight_cm{i}"] = [random.randint(3, 40) for _ in range(n)]
        data[f"MowingConditioner{i}"] = [random.choice(conditioner) for _ in range(n)]
    return pd.DataFrame(data)

# generate the dataframe
df_fake = fake_data(n_rows)

# Save the DataFrame to an Excel file
df_fake.to_excel("./DATA//ORIGINAL_DATA//fake_data.xlsx", index=False)
