import pandas as pd
import numpy as np

"""
This script processes and transforms the 'shorti_data' CSV file into a format 
similar to the 'test_data2024' Excel file. The following steps and transformations 
are performed:

1. Load the 'shorti_data' and 'test_data2024' files.
2. Define translation rules for specific mower types.
3. Filter out rows where the 'Mower' column is empty or contains invalid values 
   (such as "no", "an", "NO", "NA", "No", "Na", "", or None).
4. Filter out rows where 'M_Haeufigkeit_Vorjahr' contains non-finite values (NA or inf).
5. Split the 'Mower' column, and if it contains more than one mower type, only 
   consider the first one, applying the translation rules where necessary.
6. Create a new DataFrame and populate it with the necessary columns:
   - 'EP_PlotID': Mapped from 'PlotID'.
   - 'Year': Mapped from 'Beprobung', decreased by one year.
   - 'Cuts': Mapped from 'M_Haeufigkeit_Vorjahr', rounded up to the next integer.
   - 'MowingMachine1': Mapped from the first translated 'Mower'.
   - 'CutHeight_cm1': Mapped from 'Height_cm', with invalid or missing values set to 10.
   - 'CutWidth_m1': Mapped from 'Width_cm', with invalid or missing values set to 1.5.
   - 'MowingConditioner1': Set to the default value of 0.5.
7. Fill additional columns ('CutHeight_cm2' to 'CutHeight_cm5', 'CutWidth_m2' to 'CutWidth_m5', 
   and 'MowingConditioner2' to 'MowingConditioner5') with None.
8. Discard rows where 'MowingMachine1' or 'Mower' is empty after processing.
9. Save the transformed DataFrame to a new CSV file.

Authors: Sebastian Raubitzek, Margherita Hartlieb
"""

# Load the provided data files
shorti_data = pd.read_csv('./translate_data/test_date_2022_csv.csv')
#test_data2024 = pd.read_excel('/mnt/data/test_data2024.xlsx')

# Define the translation rules for mowers
mower_translation = {
    "Motorsense": "Sense",
    "Handrasenmäher": "Sense",
    "Aufsitzrasenmäher": "Sense",
    "Maehroboter": "Sense",
    "Rasenmäher": "Sense"
}

# Default values
Average_CutHeight_cm1 = 10
Average_CutWidth_m1 = 1.5
Average_MowingConditioner1 = 0.5

def translate_mower(mower):
    """Translate mower types based on predefined rules."""
    return mower_translation.get(mower, mower)

def split_mowers(row):
    """Split the 'Mower' column and only take the first entry, then apply translations."""
    if pd.isna(row['Mower']):
        return pd.Series([None], index=['MowingMachine1'])
    mowers = row['Mower'].split(', ')
    first_mower = mowers[0] if mowers else None
    translated_mower = translate_mower(first_mower)
    return pd.Series([translated_mower], index=['MowingMachine1'])

# Filter out rows where Mower is empty or contains invalid values
invalid_values = ["no", "an", "NO", "NA", "No", "Na", "", None]
shorti_data_filtered = shorti_data[~shorti_data['Mower'].str.lower().isin(invalid_values)]

# Filter out rows where 'M_Haeufigkeit_Vorjahr' contains non-finite values
shorti_data_filtered = shorti_data_filtered.dropna(subset=['M_Haeufigkeit_Vorjahr'])
shorti_data_filtered = shorti_data_filtered[np.isfinite(shorti_data_filtered['M_Haeufigkeit_Vorjahr'])]

# Split the mowers into separate columns and apply translation
mower_split = shorti_data_filtered.apply(split_mowers, axis=1)

# Create a dataframe for mapping
mapped_data = pd.DataFrame()

# Populate the mapped data with PlotID and Year (year reduced by one)
mapped_data['EP_PlotID'] = shorti_data_filtered['PlotID']
mapped_data['Year'] = pd.to_datetime(shorti_data_filtered['Beprobung'], dayfirst=True).dt.year - 1
mapped_data['Cuts'] = np.ceil(shorti_data_filtered['M_Haeufigkeit_Vorjahr']).astype(int)

# Map the Mowers
mapped_data = pd.concat([mapped_data, mower_split], axis=1)

# Map the Heights and Widths, handle invalid height and width values
mapped_data['CutHeight_cm1'] = shorti_data_filtered['Height_cm'].apply(
    lambda x: Average_CutHeight_cm1 if str(x).lower() in invalid_values else x
).fillna(Average_CutHeight_cm1)
mapped_data['CutWidth_m1'] = shorti_data_filtered['Width_cm'].apply(
    lambda x: Average_CutWidth_m1 if str(x).lower() in invalid_values else x
).fillna(Average_CutWidth_m1)

# Add the MowingConditioner1 with default value
mapped_data['MowingConditioner1'] = Average_MowingConditioner1

# Fill the additional columns with NaN or default values
for i in range(2, 6):
    mapped_data[f'CutHeight_cm{i}'] = None
    mapped_data[f'CutWidth_m{i}'] = None
    mapped_data[f'MowingConditioner{i}'] = None

# Discard rows where 'MowingMachine1' is empty after processing
mapped_data = mapped_data.dropna(subset=['MowingMachine1'])

# Save the final mapped data to a CSV file
output_file_path = './translate_data/translated_test_data_2022_csv.csv'
mapped_data.to_csv(output_file_path, index=False)
