import numpy as np
import pandas as pd
from copy import deepcopy as dc
import sys

def annual_trend(data_pd, Mi):
    years_list_mean = list()
    years_list_std_dev = list()
    time_stamp_list = list()
    data_pd_in = dc(data_pd)

    for i in range(1980, 2130):
        ii=0
        switch = False
        year_list = list()
        for index, row in data_pd_in.iterrows():
            if row['Year'] == i:
                year_list.append(Mi[ii])
                time_stamp_list.append(i)
                switch = True
            ii = ii + 1
        if switch:
            years_list_mean.append(np.mean(np.array(year_list, dtype=float)))
            years_list_std_dev.append(np.std(np.array(year_list, dtype=float)))

    time_stamp = np.unique(np.array(time_stamp_list))
    years_mean = np.array(years_list_mean)
    years_std_dev = np.array(years_list_std_dev)
    out_arr = np.empty((len(years_mean),3))
    out_arr[:,0] = dc(time_stamp)
    out_arr[:,1] = dc(years_mean)
    out_arr[:,2] = dc(years_std_dev)
    return out_arr


def annual_trend_single(data_pd, Mi, first_letter = None):
    years_list_mean = list()
    years_list_std_dev = list()
    #time_stamp_list = list()
    data_pd_in = dc(data_pd)
    ID_list = list()
    single_path_list = list()
    single_time_list = list()
    if first_letter == None:
        for index, row in data_pd_in.iterrows():
            #print(row['EP_PlotID'])
            curr_ID = str(row['EP_PlotID'])
            if (curr_ID in ID_list):
                print('already in list')
            else:
                #print("new ID found")
                ID_list.append(curr_ID)
    else:
        for index, row in data_pd_in.iterrows():
            curr_ID = str(row['EP_PlotID'])
            if (curr_ID in ID_list):
                print('already in list')
            else:
                if curr_ID[0] == first_letter:
                    #print("new letter ID found")
                    ID_list.append(curr_ID)
    #print(ID_list)
    ID_list = np.array(ID_list, dtype=str)
    for ID in ID_list:
        year_list = list()
        time_stamp_list = list()
        for i in range(1980, 2030):
            ii=0
            switch = False
            for index, row in data_pd_in.iterrows():
                if row['Year'] == i:
                    if row['EP_PlotID'] == ID:
                        #print('found')
                        #print(ID)
                        year_list.append(Mi[ii])
                        time_stamp_list.append(i)
                ii = ii + 1
        single_path_list.append(year_list)
        single_time_list.append(time_stamp_list)

    return single_path_list, single_time_list


def annual_trend_regional(data_pd, Mi, first_letter):
    years_list_mean = list()
    years_list_std_dev = list()
    time_stamp_list = list()
    data_pd_in = dc(data_pd)

    for i in range(1980, 2030):
        ii=0
        switch = False
        year_list = list()
        for index, row in data_pd_in.iterrows():
            if row['Year'] == i:
                reg_str = dc(str(row['EP_PlotID']))
                if reg_str[0] == first_letter:
                    print(reg_str)
                    print('found')
                    year_list.append(Mi[ii])
                    time_stamp_list.append(i)
                    switch = True
            ii = ii + 1
        if switch:
            years_list_mean.append(np.mean(np.array(year_list, dtype=float)))
            years_list_std_dev.append(np.std(np.array(year_list, dtype=float)))

    time_stamp = np.unique(np.array(time_stamp_list))
    years_mean = np.array(years_list_mean)
    years_std_dev = np.array(years_list_std_dev)
    out_arr = np.empty((len(years_mean),3))
    out_arr[:,0] = dc(time_stamp)
    out_arr[:,1] = dc(years_mean)
    out_arr[:,2] = dc(years_std_dev)
    #print(out_arr)
    return out_arr


def annual_trend_vars(year_list, EP_Plot_list, MowingMachine_list, MowingConditioner_list, CutWidth_list,
                          CutHeight_list, first_letter=None):
    return 'pizza'



def annual_trend_single_variable(data_pd, first_letter = None):
    """
    get annual trends for the single variables, i.e. mowingmachine etc.
    """
    data_pd_in = dc(data_pd)
    year_list = list()
    for i in range(1980, 2030):
        #print(i)
        switch_out = False
        MowingMachine_aux_list = list()
        MowingConditioner_aux_list = list()
        CutWidth_aux_list = list()
        CutHeight_aux_list = list()
        year_aux_list = list()
        for index, row in data_pd_in.iterrows():
            curr_ID = str(row['EP_PlotID'])
            #print(row['Year'] )
            if row['Year'] == i:
                switch = False
                if first_letter == None:
                    switch = True
                    switch_out = True
                else:
                    if curr_ID[0] == first_letter:
                        switch = True
                        switch_out = True
                #print(switch)
                if switch:
                    MowingMachine_aux_list.append(row['MowingMachine1'])
                    MowingConditioner_aux_list.append(row['MowingConditioner1'])
                    CutWidth_aux_list.append(row['CutWidth_m1'])
                    CutHeight_aux_list.append(row['CutHeight_cm1'])
        if switch_out:
            #print(i)
            #print('append')
            year_aux_list.append(i)
            year_aux_list.append(np.mean(MowingMachine_aux_list))
            year_aux_list.append(np.std(MowingMachine_aux_list))
            year_aux_list.append(np.mean(MowingConditioner_aux_list))
            year_aux_list.append(np.std(MowingConditioner_aux_list))
            year_aux_list.append(np.mean(CutWidth_aux_list))
            year_aux_list.append(np.std(CutWidth_aux_list))
            year_aux_list.append(np.mean(CutHeight_aux_list))
            year_aux_list.append(np.std(CutHeight_aux_list))
            #print(year_aux_list)
            year_list.append(year_aux_list)
    out_arr = np.array(year_list)
    return dc(out_arr)
