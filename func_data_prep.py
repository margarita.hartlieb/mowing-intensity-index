import numpy as np
import pandas as pd
import sys
from copy import deepcopy as dc


def clean_data_set(data_pd, years=None, keep_everything=False, discard_cat=None):
    """
    This function serves to get rid of all data sets that contain missing values and filter by years if specified.
    Also allows discarding specified categories.
    """
    data_pd_in = dc(data_pd)
    discard_cat = discard_cat if discard_cat else []

    # Convert the year to the correct format
    data_pd_in['Year'] = data_pd_in['Year'].apply(lambda x: int(str(x)[-4:]))

    # Debug: Check initial data
    print("Initial data:")
    print(data_pd_in.head())

    # Debug: Print all columns in the dataframe
    print("All columns in the dataframe:")
    print(data_pd_in.columns.tolist())

    # Ensure 'Cuts' column is present
    if 'Cuts' not in data_pd_in.columns:
        raise KeyError("The 'Cuts' column is missing from the DataFrame")

    # Create a copy of 'Cuts' column to preserve it
    #cuts_column = data_pd_in['Cuts'].copy()

    # Process each row for the given number of cuts
    for index, row in data_pd_in.iterrows():
        num_cuts = int(row['Cuts'])
        for cut in range(1, num_cuts + 1):
            if 'MowingMachine' not in discard_cat:
                data_pd_in.at[index, f'MowingMachine{cut}'] = dc(
                    set_mowing_machine_(row.get(f'MowingMachine{cut}', -1)))
            if 'MowingConditioner' not in discard_cat:
                data_pd_in.at[index, f'MowingConditioner{cut}'] = dc(
                    set_conditioner_(row.get(f'MowingConditioner{cut}', -1)))
            if 'CutWidth' not in discard_cat:
                data_pd_in.at[index, f'CutWidth_m{cut}'] = dc(set_cut_width_(row.get(f'CutWidth_m{cut}', -1)))
            if 'CutHeight' not in discard_cat:
                data_pd_in.at[index, f'CutHeight_cm{cut}'] = dc(set_cut_heigth_(row.get(f'CutHeight_cm{cut}', -1)))

    # Re-assign the 'Cuts' column back to the dataframe
    #data_pd_in['Cuts'] = cuts_column

    # Debug: Check data after processing cuts
    print("Data after processing cuts:")
    print(data_pd_in['Cuts'])
    print(data_pd_in.head())
    #sys.exit()


    # Debug: Print all columns in the dataframe
    print("All columns in the dataframe after processing cuts:")
    print(data_pd_in.columns.tolist())

    # Filter by years if specified
    if years is not None:
        start_year, end_year = years
        data_pd_in = data_pd_in[(data_pd_in['Year'] >= start_year) & (data_pd_in['Year'] <= end_year)]
        # Debug: Check data after filtering by years
        print("Data after filtering by years:")
        print(data_pd_in.head())

    print(type(data_pd_in))

    if keep_everything:
        print('asdfnk')
        data_pd_in = data_pd_in.apply(lambda row: fill_missing_values(row), axis=1)
    else:
        data_pd_in = data_pd_in.apply(lambda row: drop_rows_with_missing_values(row), axis=1)

    #print(data_pd_in.head())
    #sys.exit()

    # Ensure the result is a DataFrame
    data_pd_in = pd.DataFrame(data_pd_in)

    # Remove rows marked for dropping
    data_pd_in = data_pd_in[data_pd_in['drop_row'] == False].drop(columns=['drop_row']).reset_index(drop=True)


    # Debug: Check final cleaned data
    print("Final cleaned data:")
    print(data_pd_in.head())
    #sys.exit()

    # Debug: Print all columns in the dataframe after final cleaning
    print("All columns in the dataframe after final cleaning:")
    print(data_pd_in.columns.tolist())

    return dc(data_pd_in)


def fill_missing_values(row):
    if row['Cuts'] == 0 or row['Cuts'] == -1.0:
        row['drop_row'] = True  # Mark row for dropping
        #print('trigger')
        #print(row['Cuts'])
    else:
        row['drop_row'] = False
        print(row)
        for cut in range(1, int(row['Cuts']) + 1):
            if pd.isna(row[f'MowingMachine{cut}']) or row[f'MowingMachine{cut}'] == -1.0:
                row[f'MowingMachine{cut}'] = 1
            if pd.isna(row[f'MowingConditioner{cut}']) or row[f'MowingConditioner{cut}'] == -1.0:
                row[f'MowingConditioner{cut}'] = 0.5
            if pd.isna(row[f'CutWidth_m{cut}']) or row[f'CutWidth_m{cut}'] == -1.0:
                row[f'CutWidth_m{cut}'] = 2.55
            if pd.isna(row[f'CutHeight_cm{cut}']) or row[f'CutHeight_cm{cut}'] == -1.0:
                row[f'CutHeight_cm{cut}'] = 10
    return row

def drop_rows_with_missing_values(row):
    if row['Cuts'] == 0 or row['Cuts'] == -1:
        row['drop_row'] = True  # Mark row for dropping
    else:
        row['drop_row'] = False
        for cut in range(1, 2):
            if row[f'MowingMachine{cut}'] == -1.0 or row[f'MowingConditioner{cut}'] == -1.0 or \
                    row[f'CutWidth_m{cut}'] == -1.0 or row[f'CutHeight_cm{cut}'] == -1.0:
                row['drop_row'] = True  # Mark row for dropping
                break
    return row


def set_conditioner_(cell_in):
    if cell_in == "ja":
        return 1.0
    elif cell_in == "nein":
        return 0.0
    else:
        return -1.0


def set_mowing_machine_(cell_in):
    """
    Preprocess Mowing Machine
    Everything is turned into numbers, missing values are -1s
    """
    mapping = {
        "Kreiselmäher": 3.0,
        "Kreiselmaeher": 3.0,
        "Mulcher": 4.0,
        "Mulchgerät": 4.0,
        "Mulchgeraet": 4.0,
        "Scheibenmähwerk": 2.0,
        "Scheibenmaehwerk": 2.0,
        "Balkenmäher": 1.0,
        "Balkenmaeher": 1.0,
        "Fingermähwerk": 1.0,
        "Fingermaehwerk": 1.0,
        "Doppelmesser": 1.0,
        "Aufsitzmaeher": 1.0,
        "Aufsitzmäher": 1.0,
        "Aufsitzrasenmaeher": 1.0,
        "Aufsitzrasenmäher": 1.0,
        "Maehroboter": 1.0,
        "Handrasenmaeher": 1.0,
        "Handrasenmäher": 1.0,
        "Rasenmaeher": 1.0,
        "Rasenmäher": 1.0,
        "Motorsense": 0.0,
        "Freischneider": 1.0,
        "Sense": 0.0,
        -1: -1.0
    }
    return mapping.get(cell_in, -1.0)


def set_cut_width_(cell_in):
    """
    Preprocess CutWidth
    Everything is turned into numbers, missing values are -1s
    """
    try:
        cell_in = float(cell_in)
        if cell_in >= 0:
            return cell_in
    except:
        pass
    return -1.0


def set_cut_heigth_(cell_in):
    """
    Preprocess CutHeight
    Everything is turned into numbers, missing values are -1s
    """
    try:
        cell_in = float(cell_in)
        if cell_in >= 0:
            return int(cell_in)
    except:
        pass
    return -1.0


def augment_consecutive_data_(data_pd):
    """
    If e.g. the mowing machine is just set for the first cut, but it was cut 2 times, then this function will use the same mowing machine for all cuts
    """

    if isinstance(data_pd, pd.Series):
        data_pd = data_pd.to_frame().T



    data_pd_in = dc(data_pd)

    # Debug: Check final cleaned data
    print("Final cleaned data:")
    print(data_pd_in.head())

    print(data_pd_in.head())

    # Ensure 'Cuts' column is present
    if 'Cuts' not in data_pd_in.columns:
        raise KeyError("The 'Cuts' column is missing from the DataFrame")

    for index, row in data_pd_in.iterrows():
        for cut in range(2, int(row['Cuts']) + 1):
            if f'MowingMachine{cut}' in row and row[f'MowingMachine{cut}'] == -1:
                data_pd_in.at[index, f'MowingMachine{cut}'] = dc(row[f'MowingMachine{cut - 1}'])
            if f'MowingConditioner{cut}' in row and row[f'MowingConditioner{cut}'] == -1:
                data_pd_in.at[index, f'MowingConditioner{cut}'] = dc(row[f'MowingConditioner{cut - 1}'])
            if f'CutWidth_m{cut}' in row and row[f'CutWidth_m{cut}'] == -1:
                data_pd_in.at[index, f'CutWidth_m{cut}'] = dc(row[f'CutWidth_m{cut - 1}'])
            if f'CutHeight_cm{cut}' in row and row[f'CutHeight_cm{cut}'] == -1:
                data_pd_in.at[index, f'CutHeight_cm{cut}'] = dc(row[f'CutHeight_cm{cut - 1}'])

    # Debug: Check final cleaned data
    print("Final cleaned data:")
    print(data_pd_in.head())

    #sys.exit()

    return dc(data_pd_in)

