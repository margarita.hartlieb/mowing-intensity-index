import pandas as pd
import numpy as np

# Load the provided data file
shorti_data = pd.read_csv('./translate_data/test_date_2022_csv.csv')

# Define the translation rules for mowers
mower_translation = {
    "Motorsense": "Sense",
    "Handrasenmäher": "Sense",
    "Aufsitzrasenmäher": "Sense",
    "Maehroboter": "Sense",
    "Rasenmäher": "Sense"
}

# Default values
Average_CutHeight_cm1 = 10
Average_CutWidth_m1 = 1.5
Average_MowingConditioner1 = 0.5

def translate_mower(mower):
    """Translate mower types based on predefined rules."""
    return mower_translation.get(mower, mower)

def split_mowers(row):
    """Split the 'Mower' column and only take the first entry, then apply translations."""
    if pd.isna(row['Mower']):
        return pd.Series([None], index=['MowingMachine1'])
    mowers = row['Mower'].split('/')
    first_mower = mowers[0] if mowers else None
    second_mower = mowers[1] if len(mowers) > 1 else first_mower
    translated_mower_1 = translate_mower(first_mower)
    translated_mower_2 = translate_mower(second_mower)
    return pd.Series([translated_mower_1, translated_mower_2], index=['MowingMachine1', 'MowingMachine2'])

def clean_frequency(value):
    """Clean the frequency values by handling ranges and setting empty values to -1."""
    if pd.isna(value) or value in ["", " ", "NA", "Na", "na"]:
        return -1
    if isinstance(value, str) and '-' in value:
        parts = value.split('-')
        try:
            return max(float(parts[0]), float(parts[1]))
        except ValueError:
            return -1
    try:
        return float(value)
    except ValueError:
        return -1

# Filter out rows where Mower is empty or contains invalid values
invalid_values = ["no", "an", "NO", "NA", "No", "Na", "", None]
shorti_data_filtered = shorti_data[~shorti_data['Mower'].str.lower().isin(invalid_values)]

# Clean the frequency columns
shorti_data_filtered['M_Haeufigkeit_Vorjahr'] = shorti_data_filtered['M_Haeufigkeit_Vorjahr'].apply(clean_frequency)
shorti_data_filtered['M_Haeufigkeit_Beprobung'] = shorti_data_filtered['M_Haeufigkeit_Beprobung'].apply(clean_frequency)

# Filter out rows where 'M_Haeufigkeit_Vorjahr' and 'M_Haeufigkeit_Beprobung' contains non-finite values
shorti_data_filtered = shorti_data_filtered.dropna(subset=['M_Haeufigkeit_Vorjahr', 'M_Haeufigkeit_Beprobung'])
shorti_data_filtered = shorti_data_filtered[np.isfinite(shorti_data_filtered['M_Haeufigkeit_Vorjahr'])]
shorti_data_filtered = shorti_data_filtered[np.isfinite(shorti_data_filtered['M_Haeufigkeit_Beprobung'])]

# Split the mowers into separate columns and apply translation
mower_split = shorti_data_filtered.apply(split_mowers, axis=1)

# Create a dataframe for mapping
mapped_data = pd.DataFrame()

# Populate the mapped data with PlotID and Year (year reduced by one)
mapped_data['EP_PlotID'] = shorti_data_filtered['PlotID']
mapped_data['Year'] = pd.to_datetime(shorti_data_filtered['Beprobung'], dayfirst=True).dt.year - 1
mapped_data['Cuts'] = np.ceil(shorti_data_filtered['M_Haeufigkeit_Vorjahr']).astype(int)

# Map the Mowers
mapped_data = pd.concat([mapped_data, mower_split], axis=1)

# Map the Heights and Widths, handle invalid height and width values
mapped_data['CutHeight_cm1'] = shorti_data_filtered['Height_cm'].apply(
    lambda x: Average_CutHeight_cm1 if str(x).lower() in invalid_values else x
).fillna(Average_CutHeight_cm1)
mapped_data['CutWidth_m1'] = shorti_data_filtered['Width_cm'].apply(
    lambda x: Average_CutWidth_m1 if str(x).lower() in invalid_values else x
).fillna(Average_CutWidth_m1)

# Add the MowingConditioner1 with default value
mapped_data['MowingConditioner1'] = Average_MowingConditioner1

# Fill the additional columns with NaN or default values
for i in range(2, 6):
    mapped_data[f'CutHeight_cm{i}'] = None
    mapped_data[f'CutWidth_m{i}'] = None
    mapped_data[f'MowingConditioner{i}'] = None

# Discard rows where 'MowingMachine1' is empty after processing
mapped_data = mapped_data.dropna(subset=['MowingMachine1'])

# Add data for the year 2022
mapped_data_2022 = mapped_data.copy()
mapped_data_2022['Year'] = mapped_data_2022['Year'] + 1
mapped_data_2022['Cuts'] = np.ceil(shorti_data_filtered['M_Haeufigkeit_Beprobung']).astype(int)
mapped_data_2022['MowingMachine1'] = mower_split['MowingMachine2']

# Combine the data for both years
final_mapped_data = pd.concat([mapped_data, mapped_data_2022])

# Save the final mapped data to a CSV file
output_file_path = './translate_data/translated_test_data_2022_csv.csv'
final_mapped_data.to_csv(output_file_path, index=False)
