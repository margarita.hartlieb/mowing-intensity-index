import os

def create_folder_structure(workingtitle):
    # create folder structure
    if not os.path.exists('./DATA'):
        os.makedirs('./DATA')
    if not os.path.exists('./DATA/' + str(workingtitle)):
        os.makedirs('./DATA/' + str(workingtitle))
    if not os.path.exists('./DATA//ORIGINAL_DATA'):
        os.makedirs('./DATA//ORIGINAL_DATA')
    if not os.path.exists('./ANALYSIS'):
        os.makedirs('./ANALYSIS')
    if not os.path.exists('./ANALYSIS/' + str(workingtitle)):
        os.makedirs('./ANALYSIS/' + str(workingtitle))
    if not os.path.exists('./PLOTS'):
        os.makedirs('./PLOTS')
    if not os.path.exists('./PLOTS/' + str(workingtitle)):
        os.makedirs('./PLOTS/' + str(workingtitle))
    print("\n" + "folders created" + "\n")
